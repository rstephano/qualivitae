package br.com.qualivitae.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.qualivitae.dao.AlunoDAO;
import br.com.qualivitae.dao.GrupoDAO;
import br.com.qualivitae.dao.NivelDAO;
import br.com.qualivitae.dao.TreinoDAO;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Nivel;
import br.com.qualivitae.model.Treino;
import br.com.qualivitae.model.TreinoRealizado;

@Controller
@RequestMapping("/aluno/*")
public class AlunoController {

	@RequestMapping(value = "cadastro")
	public String formCadastro() {
		return "aluno/cadastro";
	}

	@RequestMapping(value = "cadastro", method = RequestMethod.POST)
	public String cadastrarAluno(@Valid Aluno aluno, BindingResult result,
			ModelMap modelMap) {
		if (result.hasErrors()) {
			return "aluno/cadastro";
		}
		AlunoDAO alunoDao = new AlunoDAO();
		alunoDao.inserir(aluno);
		return "aluno/cadastro-realizado";
	}

	@RequestMapping(value = "todos")
	public String listarAlunosTodos(ModelMap modelMap) {
		List<Aluno> alunos = new AlunoDAO().findAll();
		modelMap.addAttribute("alunos", alunos);
		return "aluno/listar-todos";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "{id}")
	public String visualizarAluno(@PathVariable("id") int id, ModelMap modelMap) {
		List<Nivel> niveis = new NivelDAO().findAll();
		modelMap.addAttribute("niveis", niveis);
		List<Grupo> grupos = new GrupoDAO().findAll();
		modelMap.addAttribute("grupos", grupos);
		Aluno aluno = new AlunoDAO().find(id);
		modelMap.addAttribute("aluno", aluno);
		int amount = 10;
		// Pegar o n�vel
		Nivel nivel = new NivelDAO().find(aluno.getNivel().getIdNivel());
		modelMap.addAttribute("nivel", nivel);
		// Pegar o grupo
		Grupo grupo = new GrupoDAO().find(aluno.getGrupo().getIdGrupo());
		modelMap.addAttribute("grupo", grupo);
		// Pegar a data in�cio
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.add(Calendar.DATE, - amount);
		modelMap.addAttribute("dataInicio", dataInicio);
		// Pegar a data atual
		Calendar hoje = Calendar.getInstance();
		modelMap.addAttribute("hoje", hoje);
		// Listar treinos
		List<Treino> treinos = new ArrayList<Treino>();
		TreinoDAO treinoDAO = new TreinoDAO();
		treinos = treinoDAO.listaTreinos(aluno.getGrupo().getIdGrupo(), aluno.getNivel().getIdNivel(), hoje, amount);
		modelMap.addAttribute("treinos", treinos);
		// Listar treinos realizados
		List<TreinoRealizado> treinosRealizados = new ArrayList<TreinoRealizado>();
		TreinoDAO treinoDAO2 = new TreinoDAO();
		treinosRealizados = treinoDAO2.listaTreinosRealizados(aluno, aluno.getGrupo().getIdGrupo(), aluno.getNivel().getIdNivel(), hoje, amount);
		modelMap.addAttribute("treinosRealizados", treinosRealizados);
		return "aluno/visualizar";
	}

	@RequestMapping(value = "{id}", method = RequestMethod.POST)
	public String atualizarAluno(@Valid Aluno aluno, BindingResult result,
			@PathVariable("id") int id, ModelMap modelMap) {
		if (result.hasErrors()) {
			if (result.getFieldValue("pessoa.senha") == "" & result.getErrorCount() == 2) {
				if (aluno.getGrupo() != null) {
					GrupoDAO grupoDAO = new GrupoDAO();
					aluno.setGrupo(grupoDAO.find(aluno.getGrupo().getIdGrupo()));
				}
				NivelDAO nivelDAO = new NivelDAO();
				aluno.setNivel(nivelDAO.find(aluno.getNivel().getIdNivel()));
				AlunoDAO alunoDao = new AlunoDAO();
				Aluno alunoAntigo = alunoDao.find(aluno.getIdAluno());
				aluno.getPessoa().setSenha(alunoAntigo.getPessoa().getSenha());
				alunoDao.atualizar(aluno);
				return "redirect:/aluno/" + id;
			} else {
				List<Nivel> niveis = new NivelDAO().findAll();
				modelMap.addAttribute("niveis", niveis);
				List<Grupo> grupos = new GrupoDAO().findAll();
				modelMap.addAttribute("grupos", grupos);
				return "aluno/visualizar";
			}
		} else {
			if (aluno.getGrupo() != null) {
				GrupoDAO grupoDAO = new GrupoDAO();
				aluno.setGrupo(grupoDAO.find(aluno.getGrupo().getIdGrupo()));
			}
			NivelDAO nivelDAO = new NivelDAO();
			aluno.setNivel(nivelDAO.find(aluno.getNivel().getIdNivel()));
			AlunoDAO alunoDao = new AlunoDAO();
			alunoDao.atualizar(aluno);
			return "redirect:/aluno/" + id;
		}
	}
	
	@RequestMapping(value = "deletar/{id}")
	public String deletarAluno(@PathVariable("id") int id) {
		AlunoDAO alunoDao = new AlunoDAO();
		alunoDao.deletar(id);
		return "redirect:/aluno/todos";
	}

	@RequestMapping(value = "addgrupo", method = RequestMethod.POST)
	public String adicionaAlunoAoGrupo(Aluno aluno) {
		Grupo grupo = new GrupoDAO().find(aluno.getGrupo().getIdGrupo());
		AlunoDAO alunoDao = new AlunoDAO();
		aluno = alunoDao.find(aluno.getIdAluno());
		aluno.setGrupo(grupo);
		alunoDao.atualizar(aluno);
		return "redirect:/grupo/" + aluno.getGrupo().getIdGrupo();
	}

	@RequestMapping(value = "removergrupo/{id}/{grupo}")
	public String deletarAluno(@PathVariable("id") int id,
			@PathVariable("grupo") int grupo) {
		AlunoDAO alunoDao = new AlunoDAO();
		Aluno aluno = alunoDao.find(id);
		aluno.setGrupo(null);
		alunoDao.atualizar(aluno);
		return "redirect:/grupo/" + grupo;
	}
}
