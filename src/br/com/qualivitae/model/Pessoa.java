package br.com.qualivitae.model;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Pessoas", uniqueConstraints = {@UniqueConstraint(columnNames={"login", "email"})})
public class Pessoa implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idPessoa;
	@NotEmpty(message = "Voc� deve preencher o Nome")
	private String nome;
	@Size(min = 14, max = 14, message = "O CPF deve ter 14 caracteres com pontos e tra�o")
	@NotEmpty(message = "O CPF n�o pode ser nulo")
	private String cpf;
	@NotEmpty(message = "Voc� deve preencher a Profiss�o")
	private String profissao;
	@NotEmpty(message = "� necess�rio selecionar um sexo")
	private String sexo;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Past(message = "Esta data  deve ser menor que a data atual")
	@NotNull(message = "A data deve ser preenchida, utilize o padr�o dd/mm/aaaa")
	private Calendar dataNascimento;
	@NotEmpty(message = "Voc� deve preencher o Endere�o")
	private String endereco;
	@NotEmpty(message = "Voc� deve preencher o Bairro")
	private String bairro;
	@NotEmpty(message = "Voc� deve preencher a Cidade")
	private String cidade;
	@NotEmpty(message = "Voc� deve preencher a UF")
	private String uf;
	@Size(min = 9, message = "O CEP deve ter 9 caracteres com pontos e tra�o")
	@NotEmpty(message = "O CEP deve ser preenchido")
	private String cep;
	@Size(min = 8, message = "O Login deve conter mais de 8 caracteres")
	@NotEmpty(message = "Voc� deve preencher o Login")
	private String login;
	@Size(min = 6, message = "Sua senha deve conter mais de 6 caracteres")
	@NotEmpty(message = "Voc� deve preencher a Senha")
	private String senha;
	@Email(message = "Voc� deve preencher um email v�lido")
	@NotEmpty(message = "Voc� deve preencher o Email")
	private String email;
	private String telefoneCelular;
	private String telefoneComercial;
	private String telefoneResidencial;

	@Id
	@GeneratedValue
	@Column(name = "idPessoa")
	public Integer getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	@Column(name = "nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column(name = "cpf")
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Column(name = "profissao")
	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	@Column(name = "sexo")
	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@Column(name = "dataNascimento")
	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Column(name = "endereco")
	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@Column(name = "bairro")
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@Column(name = "cidade")
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Column(name = "uf")
	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@Column(name = "cep")
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Column(name = "login")
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "senha")
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "telefoneCelular")
	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	@Column(name = "telefoneComercial")
	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	@Column(name = "telefoneResidencial")
	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

	private static String criptografar(String senha) {
		String sen = "";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
		sen = hash.toString(16);
		return sen;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idPessoa == null) ? 0 : idPessoa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (idPessoa == null) {
			if (other.idPessoa != null)
				return false;
		} else if (!idPessoa.equals(other.idPessoa))
			return false;
		return true;
	}
	
}