<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Cadastro - Passo 1</h2>
<div class="info">
	<b>Ol�, para realizar o cadastro no QualiVitae � necess�rio preencher nosso cadastro, s�o apenas dois passos. No primeiro passo voc� deve preencher seus dados pessoais</b><br>
</div>
<div class="content">
<form:form action="cadastro" method="POST" commandName="pessoa">
	<form:errors path="*">
		<div class="error">
			<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar realizar o cadastro:</b><br>
			<form:errors path="*" />
		</div>
	</form:errors>
	<ul id="formcadastro">
		<li class="formlabel"><label for="nome">Nome</label></li>
		<li class="forminput"><input class="inputform" id="nome" name="nome" type="text" value="${pessoa.nome}" size="100" maxlength="120"></li>
		<li class="formlabel"><label for="sexo">Sexo</label></li>
		<li class="forminput"><input type="radio" name="sexo" value="M" <c:if test="${pessoa.sexo == 'M'}">checked="checked"</c:if> />Masculino</li>
		<li class="forminput"><input type="radio" name="sexo" value="F" <c:if test="${pessoa.sexo == 'F'}">checked="checked"</c:if> />Feminino</li>
		<li class="formlabel"><label for="cpf">Cpf</label></li>
		<li class="forminput"><input class="inputform" id="cpf" name="cpf" type="text" value="${pessoa.cpf}" size="14" maxlength="14"></li>
		<li class="formlabel"><label for="profissao">Profiss�o</label></li>
		<li class="forminput"><input class="inputform" id="profissao" name="profissao" type="text" value="${pessoa.profissao}" size="100" maxlength="100"></li>
		<li class="formlabel"><label for="dataNascimento">Data de nascimento</label></li>
		<li class="forminput"><input class="inputform" id="dataNascimento" name="dataNascimento" type="text" value="<fmt:formatDate value="${pessoa.dataNascimento.time}" pattern="dd/MM/yyyy"/>" /></li>
		<li class="formlabel"><label for="endereco">Endere�o</label></li>
		<li class="forminput"><input class="inputform" id="endereco" name="endereco" type="text" value="${pessoa.endereco}" size="100" maxlength="150"></li>
		<li class="formlabel"><label for="bairro">Bairro</label></li>
		<li class="forminput"><input class="inputform" id="bairro" name="bairro" type="text" value="${pessoa.bairro}" size="100" maxlength="100"></li>
		<li class="formlabel"><label for="cidade">Cidade</label></li>
		<li class="forminput"><input class="inputform" id="cidade" name="cidade" type="text" value="${pessoa.cidade}" size="100" maxlength="100"></li>
		<li class="formlabel"><label for="uf">UF</label></li>
		<li class="forminput"><select id="uf" name="uf">
			<option value="AC" <c:if test="${pessoa.uf == 'AC'}">selected="selected"</c:if>>AC</option>
			<option value="AM" <c:if test="${pessoa.uf == 'AM'}">selected="selected"</c:if>>AM</option>
			<option value="AL" <c:if test="${pessoa.uf == 'AL'}">selected="selected"</c:if>>AL</option>
			<option value="AP" <c:if test="${pessoa.uf == 'AP'}">selected="selected"</c:if>>AP</option>
			<option value="BA" <c:if test="${pessoa.uf == 'BA'}">selected="selected"</c:if>>BA</option>
			<option value="CE" <c:if test="${pessoa.uf == 'CE'}">selected="selected"</c:if>>CE</option>
			<option value="DF" <c:if test="${pessoa.uf == 'DF'}">selected="selected"</c:if>>DF</option>
			<option value="ES" <c:if test="${pessoa.uf == 'ES'}">selected="selected"</c:if>>ES</option>
			<option value="GO" <c:if test="${pessoa.uf == 'GO'}">selected="selected"</c:if>>GO</option>
			<option value="MA" <c:if test="${pessoa.uf == 'MA'}">selected="selected"</c:if>>MA</option>
			<option value="MG" <c:if test="${pessoa.uf == 'MG'}">selected="selected"</c:if>>MG</option>
			<option value="MS" <c:if test="${pessoa.uf == 'MS'}">selected="selected"</c:if>>MS</option>
			<option value="MT" <c:if test="${pessoa.uf == 'MT'}">selected="selected"</c:if>>MT</option>
			<option value="PA" <c:if test="${pessoa.uf == 'PA'}">selected="selected"</c:if>>PA</option>
			<option value="PB" <c:if test="${pessoa.uf == 'PB'}">selected="selected"</c:if>>PB</option>
			<option value="PE" <c:if test="${pessoa.uf == 'PE'}">selected="selected"</c:if>>PE</option>
			<option value="PI" <c:if test="${pessoa.uf == 'PI'}">selected="selected"</c:if>>PI</option>
			<option value="PR" <c:if test="${pessoa.uf == 'PR'}">selected="selected"</c:if>>PR</option>
			<option value="RJ" <c:if test="${pessoa.uf == 'RJ'}">selected="selected"</c:if>>RJ</option>
			<option value="RN" <c:if test="${pessoa.uf == 'RN'}">selected="selected"</c:if>>RN</option>
			<option value="RO" <c:if test="${pessoa.uf == 'RO'}">selected="selected"</c:if>>RO</option>
			<option value="RS" <c:if test="${pessoa.uf == 'RS'}">selected="selected"</c:if>>RS</option>
			<option value="RR" <c:if test="${pessoa.uf == 'RR'}">selected="selected"</c:if>>RR</option>
			<option value="SE" <c:if test="${pessoa.uf == 'SE'}">selected="selected"</c:if>>SE</option>
			<option value="SC" <c:if test="${pessoa.uf == 'SC'}">selected="selected"</c:if>>SC</option>
			<option value="SP" <c:if test="${pessoa.uf == 'SP'}">selected="selected"</c:if>>SP</option>
			<option value="TO" <c:if test="${pessoa.uf == 'TO'}">selected="selected"</c:if>>TO</option>
		</select></li>
		<li class="formlabel"><label for="cep">CEP</label></li>
		<li class="forminput"><input class="inputform" id="cep" name="cep" type="text" value="${pessoa.cep}" size="9" maxlength="9"></li>
		<li class="formlabel"><label for="login">Login</label></li>
		<li class="forminput"><input class="inputform" id="login" name="login" type="text" value="${pessoa.login}" size="45" maxlength="45"></li>
		<li class="formlabel"><label for="senha">Senha</label></li>
		<li class="forminput"><input class="inputform" id="senha" name="senha" type="password" size="45" maxlength="45"></li>
		<li class="formlabel"><label for="email">Email</label></li>
		<li class="forminput"><input class="inputform" id="email" name="email" type="text" value="${pessoa.email}" size="100" maxlength="100"></li>
		<li class="formlabel"><label for="telefoneCelular">Celular</label></li>
		<li class="forminput"><input class="inputform" id="telefoneCelular" name="telefoneCelular" type="text" value="${pessoa.telefoneCelular}" size="15" maxlength="15"></li>
		<li class="formlabel"><label for="telefoneComercial">Telefone comercial</label></li>
		<li class="forminput"><input class="inputform" id="telefoneComercial" name="telefoneComercial" type="text" value="${pessoa.telefoneComercial}" size="15" maxlength="15"></li>
		<li class="formlabel"><label for="telefoneResidencial">Telefone residencial</label></li>
		<li class="forminput"><input class="inputform" id="telefoneResidencial" name="telefoneResidencial" type="text" value="${pessoa.telefoneResidencial}" size="15" maxlength="15"></li>
		<li>
			<button class="enviar">Pr�ximo >></button>
		</li>
	</ul>
</form:form>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>