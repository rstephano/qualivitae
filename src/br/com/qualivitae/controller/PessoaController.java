package br.com.qualivitae.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.qualivitae.dao.PessoaDAO;
import br.com.qualivitae.model.Pessoa;

@Controller
public class PessoaController {
	@RequestMapping(value = "cadastro")
	public String formCadastro() {
		return "pessoa/cadastro";
	}

	//@RequestMapping(value = "cadastro", method = RequestMethod.POST)
	public String cadastrar(@Valid Pessoa pessoa, BindingResult result, ModelMap modelMap) {
		if (result.hasErrors()) {
			return "pessoa/cadastro";
		}
		PessoaDAO pessoaDao = new PessoaDAO();
		pessoaDao.inserir(pessoa);
		return "pessoa/cadastro";
	}
}
