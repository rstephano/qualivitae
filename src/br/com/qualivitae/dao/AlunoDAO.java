package br.com.qualivitae.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Pessoa;
import br.com.qualivitae.model.Treino;

public class AlunoDAO {
	private final Session session;

	public AlunoDAO() {
		this.session = CriadorDeSession.getSession();
	}

	public Integer inserir(Aluno aluno) {
		Transaction tx = session.beginTransaction();
		session.save(aluno);
		tx.commit();
		session.close();
		return aluno.getIdAluno();
	}

	public Aluno find(int id) {
		session.beginTransaction();
		Aluno aluno = (Aluno) session.get(Aluno.class, id);
		session.getTransaction().commit();
		return aluno;
	}

	public List<Aluno> findAll() {
		session.beginTransaction();
		List<Aluno> alunos = session.createQuery("from Aluno").list();
		session.getTransaction().commit();
		return alunos;
	}

	public List<Aluno> findAllInGroup(int grupo) {
		session.beginTransaction();
		List<Aluno> alunos = session.createQuery(
				"from Aluno where idGrupo=" + grupo).list();
		session.getTransaction().commit();
		return alunos;
	}

	public List<Aluno> findAllNotInGroup() {
		session.beginTransaction();
		List<Aluno> alunos = session.createQuery(
				"from Aluno where idGrupo is null ").list();
		session.getTransaction().commit();
		return alunos;
	}

	public void atualizar(Aluno aluno) {
		Transaction tx = session.beginTransaction();
		session.clear();
		session.update(aluno);
		tx.commit();
	}

	public void deletar(int id) {
		Aluno aluno = new AlunoDAO().find(id);
		session.beginTransaction();
		session.delete(aluno);
		session.getTransaction().commit();
	}

	public Aluno find(String login, String senha) {
		session.beginTransaction();
		Pessoa pessoa = new PessoaDAO().find(login, senha);
		List<Aluno> alunos = session.createQuery("from Aluno where idPessoa=" + pessoa.getIdPessoa()).list();
		Aluno aluno = new Aluno();
		for (Aluno alunotemp : (List<Aluno>) alunos) {
			aluno = alunotemp;
		}
		session.getTransaction().commit();
		return aluno;
	}
}
