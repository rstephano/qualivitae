<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>QualiVitae</title>
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jquery-ui-1.8.20.custom.min.js"></script>
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jquery.ui.datepicker-pt-BR.js"></script>
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/resources/js/sliding_effect.js"></script>
<script>
	$(function() {
		$.datepicker.setDefaults($.datepicker.regional["pt-BR"]);
		$(".datepicker").datepicker({
			maxDate : "+0D",
			changeMonth : true,
			changeYear : true
		});
	});
</script>

<c:set var="now" value="<%=new java.util.Date()%>" />

<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/reset.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/960.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/960_16_col.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/text.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/forms.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/smoothness/jquery-ui-1.8.20.custom.css">
</head>
<body>
	<div class="container_16" id="top">
		<div class="grid_4" id="logo"></div>
		<div class="grid_7">&nbsp;</div>
		<div class="grid_5">
			<c:choose>
				<c:when test="${alunoLogado.idAluno != null}">
					<p class="usertoolbar"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"> ${alunoLogado.pessoa.nome} <a href="<%=request.getContextPath()%>/login/logout"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/10/151.png"></a></p>
				</c:when>
				<c:when test="${professorLogado.idProfessor != null}">
					<p class="usertoolbar"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"> ${professorLogado.pessoa.nome} <a href="<%=request.getContextPath()%>/login/logout"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/10/151.png"></a></p>
				</c:when>
				<c:otherwise>
					<a href="<%=request.getContextPath()%>/cadastro" class="join">Cadastre-se agora!</a>
					<a class="loginlink" href="<%=request.getContextPath()%>/login">Ou efetue o Login</a>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="clear"></div>
		<div class="grid_4" id="col_esq">
			<ul id="sliding-navigation">
				<c:if test="${alunoLogado != null}">
					<li class="sliding-element"><h3>Menu de aluno</h3></li>
					<li class="sliding-element"><a href="<%=request.getContextPath()%>/index">In�cio</a></li>
					<li class="sliding-element"><a href="<%=request.getContextPath()%>/aluno/${alunoLogado.idAluno}">Meu perfil</a></li>
					<li class="sliding-element"><a href="<%=request.getContextPath()%>/treino/${alunoLogado.grupo.idGrupo}-${alunoLogado.nivel.idNivel}?mes=<fmt:formatDate value="${now}" pattern="MM" />&ano=<fmt:formatDate value="${now}" pattern="yyyy" />">Meu treino</a></li>
				</c:if>
				<c:if test="${professorLogado != null}">
					<li class="sliding-element"><h3>Menu de Professor</h3></li>
					<li class="sliding-element"><a href="<%=request.getContextPath()%>/index">In�cio</a></li>
					<li class="sliding-element"><a href="<%=request.getContextPath()%>/professor/${professorLogado.idProfessor}">Meu perfil</a></li>
					<li class="sliding-element"><a href="<%=request.getContextPath()%>/grupo/todos">Meus Grupos</a></li>
					<c:if test="${professorLogado.administrador != null}">
						<li class="sliding-element"><h3>Menu de Administrador</h3></li>
						<li class="sliding-element"><a href="<%=request.getContextPath()%>/professor/todos">Professores</a></li>
						<li class="sliding-element"><a href="<%=request.getContextPath()%>/aluno/todos">Alunos</a></li>
						<li class="sliding-element"><a href="<%=request.getContextPath()%>/grupo/cadastro">Cadastro de Grupos</a></li>
					</c:if>
				</c:if>
			</ul>
		</div>
		<div class="grid_12" id="content">