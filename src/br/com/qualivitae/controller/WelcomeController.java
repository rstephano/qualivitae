package br.com.qualivitae.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.qualivitae.dao.GrupoDAO;
import br.com.qualivitae.dao.NivelDAO;
import br.com.qualivitae.dao.TreinoDAO;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Nivel;
import br.com.qualivitae.model.Professor;
import br.com.qualivitae.model.Treino;
import br.com.qualivitae.model.TreinoRealizado;

@Controller
@SessionAttributes({"alunoLogado"})
public class WelcomeController {
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(ModelMap modelMap, HttpSession session) {
		int amount = 10;
		// Aluno Logado
		Aluno aluno = (Aluno) session.getAttribute("alunoLogado");
		if (aluno != null) {
			// Pegar o n�vel
			Nivel nivel = new NivelDAO().find(aluno.getNivel().getIdNivel());
			modelMap.addAttribute("nivel", nivel);
			// Pegar o grupo
			Grupo grupo = new GrupoDAO().find(aluno.getGrupo().getIdGrupo());
			modelMap.addAttribute("grupo", grupo);
		}
		// Pegar a data in�cio
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.add(Calendar.DATE, - amount);
		modelMap.addAttribute("dataInicio", dataInicio);
		// Pegar a data atual
		Calendar hoje = Calendar.getInstance();
		modelMap.addAttribute("hoje", hoje);
		if (aluno != null) {
			// Listar treinos
			List<Treino> treinos = new ArrayList<Treino>();
			TreinoDAO treinoDAO = new TreinoDAO();
			treinos = treinoDAO.listaTreinos(aluno.getGrupo().getIdGrupo(), aluno.getNivel().getIdNivel(), hoje, amount);
			modelMap.addAttribute("treinos", treinos);
			// Listar treinos realizados
			List<TreinoRealizado> treinosRealizados = new ArrayList<TreinoRealizado>();
			TreinoDAO treinoDAO2 = new TreinoDAO();
			treinosRealizados = treinoDAO2.listaTreinosRealizados(aluno, aluno.getGrupo().getIdGrupo(), aluno.getNivel().getIdNivel(), hoje, amount);
			modelMap.addAttribute("treinosRealizados", treinosRealizados);
		}
		return "index";
	}
}
