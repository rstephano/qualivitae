<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jquery.balloon.min.js"></script>
<c:forEach var="treino" items="${treinos}">
	<script>
		$(function() {
			$(".treino<fmt:formatDate value="${treino.data.time}" pattern="ddMM" />").each(function() { $(this).balloon({
				contents: '<b>${treino.tipo}</b></br>${treino.descricao}</br>Planejado: ${treino.planejado} metros', showDuration: "slow", showAnimation: function(d) { this.fadeIn(d); }
			}); });
		});
	</script>
</c:forEach>
<h2>Treinos</h2>
<div class="content">
	<p id="testes">
		<b>Grupo:</b> ${grupo.nome} / <b>N�vel:</b> ${nivel.nome}
	</p>
	<table id="calendar">
		<tr><td><a class="calendartool" href="<%=request.getContextPath()%>/treino/${grupo.idGrupo}-${nivel.idNivel}?mes=${intMesAnterior}&ano=${intAnoAnterior}"><</a></td><td class="calendarheader" colspan="5"><p>${strMesAtual} / ${intAnoAtual}</p></td><td><a class="calendartool" href="<%=request.getContextPath()%>/treino/${grupo.idGrupo}-${nivel.idNivel}?mes=${intMesPosterior}&ano=${intAnoPosterior}">></a></td></tr>
		<tr class="cabecalho">
			<td>Domingo</td>
			<td>Segunda</td>
			<td>Ter�a</td>
			<td>Quarta</td>
			<td>Quinta</td>
			<td>Sexta</td>
			<td>S�bado</td>
		</tr>
		<tr>
		<c:forEach var="dataCalendario" items="${datasCalendario}" varStatus="id">
			<td class="<c:if test="${dataCalendario.time.month == mesCorrente}">mesCorrente</c:if><c:forEach var="treino" items="${treinos}"><c:if test="${dataCalendario.time.date == treino.data.time.date && dataCalendario.time.month == treino.data.time.month && dataCalendario.time.year == treino.data.time.year}"> treino treino<fmt:formatDate value="${treino.data.time}" pattern="ddMM" /></c:if></c:forEach>">
				<c:if test="${dataCalendario.time.month == mesCorrente}">
					<a <c:if test="${dataCalendario.time.date == hoje.time.date && dataCalendario.time.month == hoje.time.month && dataCalendario.time.year == hoje.time.year}">class="hoje"</c:if> href="<%=request.getContextPath()%>/treino/${grupo.idGrupo}-${nivel.idNivel}/view?dia=<fmt:formatDate value="${dataCalendario.time}" pattern="dd" />&mes=<fmt:formatDate value="${dataCalendario.time}" pattern="MM" />&ano=<fmt:formatDate value="${dataCalendario.time}" pattern="yyyy" />">
				</c:if>
					<fmt:formatDate value="${dataCalendario.time}" pattern="dd" />
				<c:if test="${dataCalendario.time.month == mesCorrente}"></a></c:if>
			</td>
			${id.count % 7 == 0 ? '</tr><tr>' : ''}
		</c:forEach>
		</tr>
	</table>
	<p>Legenda</p>
	<p><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/074.png"> Dia de treino</p>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>