package br.com.qualivitae.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Pessoa;

public class PessoaDAO {
	private final Session session;

	public PessoaDAO() {
		this.session = CriadorDeSession.getSession();
	}

	public Integer inserir(Pessoa pessoa) {
		Transaction tx = session.beginTransaction();
		session.save(pessoa);
		tx.commit();
		session.close();
		return pessoa.getIdPessoa();
	}

	public Pessoa find(String login, String senha) {
		session.beginTransaction();
		List<Pessoa> pessoas = session.createQuery("from Pessoa where (login = '" + login + "') and (senha ='" + senha +"')").list();
		Pessoa pessoa = new Pessoa();
		for (Pessoa pessoaTemp : (List<Pessoa>) pessoas) {
			pessoa = (Pessoa) session.get(Pessoa.class, pessoaTemp.getIdPessoa());
		}
		session.getTransaction().commit();
		return pessoa;
	}

}
