<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Cadastro - Passo 2</h2>
<div class="info">
	<b>Ol� ${pessoa.nome}, no segundo passo voc� deve escolher entre cadastro de professor ou aluno.</b><br>
</div>
<script>
	$(function() {
		$("#tabs").tabs();
	});
</script>
<div class="content">
	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">Aluno</a></li>
			<li><a href="#tabs-2">Professor</a></li>
		</ul>
		<div id="tabs-1">
			<form:form action="cadastrar" method="POST" commandName="aluno">
				<form:errors path="*">
					<div class="error">
						<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar realizar o cadastro:</b><br>
						<form:errors path="*" />
					</div>
				</form:errors>
				<ul id="formcadastro">
					<input type="hidden" name="pessoa.idPessoa" value="${pessoa.idPessoa}" />
					<li class="formlabel"><label for="numCamisa">N�mero de camisa:</label></li>
					<select id="numCamisa" name="numCamisa">
						<option value="P" <c:if test="${aluno.numCamisa != 'P'}">selected="selected"</c:if>>P</option>
						<option value="M" <c:if test="${aluno.numCamisa != 'M'}">selected="selected"</c:if>>M</option>
						<option value="G" <c:if test="${aluno.numCamisa != 'G'}">selected="selected"</c:if>>G</option>
						<option value="GG" <c:if test="${aluno.numCamisa != 'GG'}">selected="selected"</c:if>>GG</option>
						<option value="XG" <c:if test="${aluno.numCamisa != 'XG'}">selected="selected"</c:if>>XG</option>
					</select>
					<li>
						<button class="enviar">Cadastrar aluno</button>
					</li>
				</ul>
			</form:form>
		</div>
		<div id="tabs-2">
			<form:form action="cadastrar" method="POST" commandName="professor">
				<form:errors path="*">
					<div class="error">
						<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar realizar o cadastro:</b><br>
						<form:errors path="*" />
					</div>
				</form:errors>
				<ul id="formcadastro">
					<input type="hidden" name="pessoa.idPessoa" value="${pessoa.idPessoa}" />
					<li class="formlabel"><label for="cref">CREF:</label></li>
					<li class="forminput"><input class="inputform" id="cref" name="cref" type="text" value="${professor.cref}" size="45" maxlength="45"></li>
					<li>
						<button class="enviar">Cadastrar professor</button>
					</li>
				</ul>
			</form:form>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>