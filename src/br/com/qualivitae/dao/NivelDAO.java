package br.com.qualivitae.dao;

import java.util.List;

import org.hibernate.Session;

import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Nivel;
public class NivelDAO {
	private final Session session;
	public NivelDAO() {
		this.session = CriadorDeSession.getSession();
	}
	public Nivel find(String id) {
		session.beginTransaction();
		Nivel nivel = (Nivel) session.get(Nivel.class, id);
		session.getTransaction().commit();
		return nivel;
	}
	public List<Nivel> findAll() {
		session.beginTransaction();
		List<Nivel> niveis = session.createQuery("from Nivel").list();
		session.getTransaction().commit();
		return niveis;
	}
}
