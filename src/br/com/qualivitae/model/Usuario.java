package br.com.qualivitae.model;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Usuarios")
public class Usuario {
	@Id
	@GeneratedValue
	private int idUsuario;
	@Column(name = "Grupos_idGrupo")
	@NotNull(message = "Voc� deve escolher um grupo")
	private int idGrupo;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Past(message = "Esta data ser menor que a data atual")
	@NotNull(message = "A data deve ser preenchida, utilize o padr�o dd/mm/aaaa")
	private Calendar dataNascimento;
	@Column
	@NotEmpty(message = "O nome deve ser preenchido")
	private String nome;
	@Column
	@Size(min = 14, max = 14, message = "O CPF deve ter 14 caracteres com pontos e tra�o")
	@NotEmpty(message = "O CPF n�o pode ser nulo")
	private String cpf;
	@Column
	@NotEmpty(message = "A profiss�o deve ser preenchida")
	private String profissao;
	@Column
	@NotEmpty(message = "O endere�o deve ser preenchido")
	private String endereco;
	@Column
	@NotEmpty(message = "O bairro deve ser preenchida")
	private String bairro;
	@Column
	@NotEmpty(message = "A cidade deve ser preenchida")
	private String cidade;
	@Column
	@Size(min = 9, message = "O CEP deve ter 9 caracteres com pontos e tra�o")
	@NotEmpty(message = "O CEP deve ser preenchido")
	private String cep;
	@Column(unique = true)
	@NotEmpty(message = "O login deve ser preenchido")
	private String login;
	@Size(min = 8, message = "A senha deve ter 8 caracteres no m�nimo")
	@NotEmpty(message = "A senha deve ser preenchida")
	private String senha;
	@Column(unique = true)
	@NotEmpty(message = "O email deve ser preenchida")
	@Email(message = "Favor informar um email v�lido")
	private String email;
	@Column
	private String telefoneCelular;
	@Column
	private String telefoneComercial;
	@Column
	private String telefoneResidencial;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}

	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}

}