<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Cadastro de Professor</h2>
<div class="info">
	<b>Ol�, para realizar o cadastro no QualiVitae � necess�rio preencher o formul�rio abaixo.</b><br>
</div>
<div class="content">
	<spring:nestedPath path="professor">
		<form:form action="" method="POST" commandName="professor">
			<form:errors path="*">
				<div class="error">
					<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar realizar o cadastro:</b><br>
					<form:errors path="*" />
				</div>
			</form:errors>
			<p><label for="pessoa.nome">Nome</label><input class="inputform" id="pessoa.nome" name="pessoa.nome" type="text" value="${professor.pessoa.nome}" size="30" maxlength="120"></p>
			<p><label for="pessoa.sexo">Sexo</label><input type="radio" name="pessoa.sexo" value="M" <c:if test="${professor.pessoa.sexo == 'M'}">checked="checked"</c:if> />Masculino<input type="radio" name="pessoa.sexo" value="F" <c:if test="${professor.pessoa.sexo == 'F'}">checked="checked"</c:if> />Feminino</p>
			<p><label for="pessoa.cpf">Cpf</label><input class="inputform" id="pessoa.cpf" name="pessoa.cpf" type="text" value="${professor.pessoa.cpf}" size="14" maxlength="14"></p>
			<p><label for="pessoa.profissao">Profiss�o</label><input class="inputform" id="pessoa.profissao" name="pessoa.profissao" type="text" value="${professor.pessoa.profissao}" size="30" maxlength="100"></p>
			<p><label for="pessoa.dataNascimento">Data de nascimento</label><input class="inputform datepicker" id="pessoa.dataNascimento" name="pessoa.dataNascimento" type="text" value="<fmt:formatDate value="${professor.pessoa.dataNascimento.time}" pattern="dd/MM/yyyy"/>" /></p>
			<p><label for="pessoa.endereco">Endere�o</label><input class="inputform" id="pessoa.endereco" name="pessoa.endereco" type="text" value="${professor.pessoa.endereco}" size="30" maxlength="150"></p>
			<p><label for="pessoa.bairro">Bairro</label><input class="inputform" id="pessoa.bairro" name="pessoa.bairro" type="text" value="${professor.pessoa.bairro}" size="30" maxlength="100"></p>
			<p><label for="pessoa.cidade">Cidade</label><input class="inputform" id="pessoa.cidade" name="pessoa.cidade" type="text" value="${professor.pessoa.cidade}" size="30" maxlength="100"></p>
			<p><label for="pessoa.uf">UF</label><select id="pessoa.uf" name="pessoa.uf">
				<option value="AC" <c:if test="${professor.pessoa.uf == 'AC'}">selected="selected"</c:if>>AC</option>
				<option value="AM" <c:if test="${professor.pessoa.uf == 'AM'}">selected="selected"</c:if>>AM</option>
				<option value="AL" <c:if test="${professor.pessoa.uf == 'AL'}">selected="selected"</c:if>>AL</option>
				<option value="AP" <c:if test="${professor.pessoa.uf == 'AP'}">selected="selected"</c:if>>AP</option>
				<option value="BA" <c:if test="${professor.pessoa.uf == 'BA'}">selected="selected"</c:if>>BA</option>
				<option value="CE" <c:if test="${professor.pessoa.uf == 'CE'}">selected="selected"</c:if>>CE</option>
				<option value="DF" <c:if test="${professor.pessoa.uf == 'DF'}">selected="selected"</c:if>>DF</option>
				<option value="ES" <c:if test="${professor.pessoa.uf == 'ES'}">selected="selected"</c:if>>ES</option>
				<option value="GO" <c:if test="${professor.pessoa.uf == 'GO'}">selected="selected"</c:if>>GO</option>
				<option value="MA" <c:if test="${professor.pessoa.uf == 'MA'}">selected="selected"</c:if>>MA</option>
				<option value="MG" <c:if test="${professor.pessoa.uf == 'MG'}">selected="selected"</c:if>>MG</option>
				<option value="MS" <c:if test="${professor.pessoa.uf == 'MS'}">selected="selected"</c:if>>MS</option>
				<option value="MT" <c:if test="${professor.pessoa.uf == 'MT'}">selected="selected"</c:if>>MT</option>
				<option value="PA" <c:if test="${professor.pessoa.uf == 'PA'}">selected="selected"</c:if>>PA</option>
				<option value="PB" <c:if test="${professor.pessoa.uf == 'PB'}">selected="selected"</c:if>>PB</option>
				<option value="PE" <c:if test="${professor.pessoa.uf == 'PE'}">selected="selected"</c:if>>PE</option>
				<option value="PI" <c:if test="${professor.pessoa.uf == 'PI'}">selected="selected"</c:if>>PI</option>
				<option value="PR" <c:if test="${professor.pessoa.uf == 'PR'}">selected="selected"</c:if>>PR</option>
				<option value="RJ" <c:if test="${professor.pessoa.uf == 'RJ'}">selected="selected"</c:if>>RJ</option>
				<option value="RN" <c:if test="${professor.pessoa.uf == 'RN'}">selected="selected"</c:if>>RN</option>
				<option value="RO" <c:if test="${professor.pessoa.uf == 'RO'}">selected="selected"</c:if>>RO</option>
				<option value="RS" <c:if test="${professor.pessoa.uf == 'RS'}">selected="selected"</c:if>>RS</option>
				<option value="RR" <c:if test="${professor.pessoa.uf == 'RR'}">selected="selected"</c:if>>RR</option>
				<option value="SE" <c:if test="${professor.pessoa.uf == 'SE'}">selected="selected"</c:if>>SE</option>
				<option value="SC" <c:if test="${professor.pessoa.uf == 'SC'}">selected="selected"</c:if>>SC</option>
				<option value="SP" <c:if test="${professor.pessoa.uf == 'SP'}">selected="selected"</c:if>>SP</option>
				<option value="TO" <c:if test="${professor.pessoa.uf == 'TO'}">selected="selected"</c:if>>TO</option>
			</select></p>
			<p><label for="pessoa.cep">CEP</label><input class="inputform" id="pessoa.cep" name="pessoa.cep" type="text" value="${professor.pessoa.cep}" size="9" maxlength="9"></p>
			<p><label for="pessoa.login">Login</label><input class="inputform" id="pessoa.login" name="pessoa.login" type="text" value="${professor.pessoa.login}" size="30" maxlength="45"></p>
			<p><label for="pessoa.senha">Senha</label><input class="inputform" id="pessoa.senha" name="pessoa.senha" type="password" size="30" maxlength="45"></p>
			<p><label for="pessoa.email">Email</label><input class="inputform" id="pessoa.email" name="pessoa.email" type="text" value="${professor.pessoa.email}" size="30" maxlength="100"></p>
			<p><label for="pessoa.telefoneCelular">Celular</label><input class="inputform" id="pessoa.telefoneCelular" name="pessoa.telefoneCelular" type="text" value="${professor.pessoa.telefoneCelular}" size="15" maxlength="15"></p>
			<p><label for="pessoa.telefoneComercial">Telefone comercial</label><input class="inputform" id="pessoa.telefoneComercial" name="pessoa.telefoneComercial" type="text" value="${professor.pessoa.telefoneComercial}" size="15" maxlength="15"></p>
			<p><label for="pessoa.telefoneResidencial">Telefone residencial</label><input class="inputform" id="pessoa.telefoneResidencial" name="pessoa.telefoneResidencial" type="text" value="${professor.pessoa.telefoneResidencial}" size="15" maxlength="15"></p>
			<p><label for="cref">CREF:</label><input class="inputform" id="cref" name="cref" type="text" value="${professor.cref}" size="30" maxlength="45"></p>
			<p class="buttons"><button class="enviar">Cadastrar Professor</button></p>
		</form:form>
	</spring:nestedPath>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>