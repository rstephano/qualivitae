<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Cadastro de ${aluno.pessoa.nome}</h2>
<div class="content">

	<c:choose>
		<c:when test="${professorLogado.idProfessor != null}">
			<div id="chart_div" style="width: 660px; height: 200px;"></div>
			<script type="text/javascript" src="https://www.google.com/jsapi"></script>
			<script type="text/javascript">
				google.load("visualization", "1", {
					packages : [ "corechart" ]
				});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
					var data = new google.visualization.DataTable();
					data.addColumn('date', 'Data');
					data.addColumn('number', 'Previsto');
					data.addColumn('number', 'Realizado');
					data.addRows([
						<c:forEach var="treino" items="${treinos}" varStatus="id">
						<c:set var="control" value="0" scope="request" />
						<c:forEach var="treinoRealizado" items="${treinosRealizados}">
							<c:if test="${treinoRealizado.treino.idTreino == treino.idTreino}">
								<c:set var="control" value="1" scope="request" />
								[new Date(<fmt:formatDate value="${treino.data.time}" pattern="yyyy" />, <fmt:formatDate value="${treino.data.time}" pattern="MM" />, <fmt:formatDate value="${treino.data.time}" pattern="dd" />), ${treino.planejado}, ${treinoRealizado.realizado}],
							</c:if>
						</c:forEach>
						<c:if test="${control == 0}">
							[new Date(<fmt:formatDate value="${treino.data.time}" pattern="yyyy" />, <fmt:formatDate value="${treino.data.time}" pattern="MM" />, <fmt:formatDate value="${treino.data.time}" pattern="dd" />), ${treino.planejado}, null],
						</c:if>
						</c:forEach>
					]);
		
					var options = {
						title : 'Hist�rico de Treinos',
						legend: 'none',
						hAxis: {title: 'Data'},
						pointSize: 5
					};
		
					var chart = new google.visualization.LineChart(document
							.getElementById('chart_div'));
					chart.draw(data, options);
				}
			</script>
		</c:when>
	</c:choose>

	<spring:nestedPath path="aluno">
		<form:form action="${pageContext.request.contextPath}/aluno/${aluno.idAluno}" method="POST" commandName="aluno">
			<form:errors path="*">
				<div class="error">
					<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar atualizar o cadastro:</b><br>
					<form:errors path="*" />
				</div>
			</form:errors>
			<input type="hidden" name="pessoa.idPessoa" value="${aluno.pessoa.idPessoa}">
			<p><label for="pessoa.nome">Nome</label><input class="inputform" id="pessoa.nome" name="pessoa.nome" type="text" value="${aluno.pessoa.nome}" size="30" maxlength="120"></p>
			<p><label for="pessoa.sexo">Sexo</label><input type="radio" name="pessoa.sexo" value="M" <c:if test="${aluno.pessoa.sexo == 'M'}">checked="checked"</c:if> />Masculino<input type="radio" name="pessoa.sexo" value="F" <c:if test="${aluno.pessoa.sexo == 'F'}">checked="checked"</c:if> />Feminino</p>
			<p><label for="pessoa.cpf">Cpf</label><input class="inputform" id="pessoa.cpf" name="pessoa.cpf" type="text" value="${aluno.pessoa.cpf}" size="14" maxlength="14"></p>
			<p><label for="pessoa.profissao">Profiss�o</label><input class="inputform" id="pessoa.profissao" name="pessoa.profissao" type="text" value="${aluno.pessoa.profissao}" size="30" maxlength="100"></p>
			<p><label for="pessoa.dataNascimento">Data de nascimento</label><input class="inputform" id="pessoa.dataNascimento" name="pessoa.dataNascimento" type="text" value="<fmt:formatDate value="${aluno.pessoa.dataNascimento.time}" pattern="dd/MM/yyyy"/>" /></p>
			<p><label for="pessoa.endereco">Endere�o</label><input class="inputform" id="pessoa.endereco" name="pessoa.endereco" type="text" value="${aluno.pessoa.endereco}" size="30" maxlength="150"></p>
			<p><label for="pessoa.bairro">Bairro</label><input class="inputform" id="pessoa.bairro" name="pessoa.bairro" type="text" value="${aluno.pessoa.bairro}" size="30" maxlength="100"></p>
			<p><label for="pessoa.cidade">Cidade</label><input class="inputform" id="pessoa.cidade" name="pessoa.cidade" type="text" value="${aluno.pessoa.cidade}" size="30" maxlength="100"></p>
			<p><label for="pessoa.uf">UF</label><select id="pessoa.uf" name="pessoa.uf">
				<option value="AC" <c:if test="${aluno.pessoa.uf == 'AC'}">selected="selected"</c:if>>AC</option>
				<option value="AM" <c:if test="${aluno.pessoa.uf == 'AM'}">selected="selected"</c:if>>AM</option>
				<option value="AL" <c:if test="${aluno.pessoa.uf == 'AL'}">selected="selected"</c:if>>AL</option>
				<option value="AP" <c:if test="${aluno.pessoa.uf == 'AP'}">selected="selected"</c:if>>AP</option>
				<option value="BA" <c:if test="${aluno.pessoa.uf == 'BA'}">selected="selected"</c:if>>BA</option>
				<option value="CE" <c:if test="${aluno.pessoa.uf == 'CE'}">selected="selected"</c:if>>CE</option>
				<option value="DF" <c:if test="${aluno.pessoa.uf == 'DF'}">selected="selected"</c:if>>DF</option>
				<option value="ES" <c:if test="${aluno.pessoa.uf == 'ES'}">selected="selected"</c:if>>ES</option>
				<option value="GO" <c:if test="${aluno.pessoa.uf == 'GO'}">selected="selected"</c:if>>GO</option>
				<option value="MA" <c:if test="${aluno.pessoa.uf == 'MA'}">selected="selected"</c:if>>MA</option>
				<option value="MG" <c:if test="${aluno.pessoa.uf == 'MG'}">selected="selected"</c:if>>MG</option>
				<option value="MS" <c:if test="${aluno.pessoa.uf == 'MS'}">selected="selected"</c:if>>MS</option>
				<option value="MT" <c:if test="${aluno.pessoa.uf == 'MT'}">selected="selected"</c:if>>MT</option>
				<option value="PA" <c:if test="${aluno.pessoa.uf == 'PA'}">selected="selected"</c:if>>PA</option>
				<option value="PB" <c:if test="${aluno.pessoa.uf == 'PB'}">selected="selected"</c:if>>PB</option>
				<option value="PE" <c:if test="${aluno.pessoa.uf == 'PE'}">selected="selected"</c:if>>PE</option>
				<option value="PI" <c:if test="${aluno.pessoa.uf == 'PI'}">selected="selected"</c:if>>PI</option>
				<option value="PR" <c:if test="${aluno.pessoa.uf == 'PR'}">selected="selected"</c:if>>PR</option>
				<option value="RJ" <c:if test="${aluno.pessoa.uf == 'RJ'}">selected="selected"</c:if>>RJ</option>
				<option value="RN" <c:if test="${aluno.pessoa.uf == 'RN'}">selected="selected"</c:if>>RN</option>
				<option value="RO" <c:if test="${aluno.pessoa.uf == 'RO'}">selected="selected"</c:if>>RO</option>
				<option value="RS" <c:if test="${aluno.pessoa.uf == 'RS'}">selected="selected"</c:if>>RS</option>
				<option value="RR" <c:if test="${aluno.pessoa.uf == 'RR'}">selected="selected"</c:if>>RR</option>
				<option value="SE" <c:if test="${aluno.pessoa.uf == 'SE'}">selected="selected"</c:if>>SE</option>
				<option value="SC" <c:if test="${aluno.pessoa.uf == 'SC'}">selected="selected"</c:if>>SC</option>
				<option value="SP" <c:if test="${aluno.pessoa.uf == 'SP'}">selected="selected"</c:if>>SP</option>
				<option value="TO" <c:if test="${aluno.pessoa.uf == 'TO'}">selected="selected"</c:if>>TO</option>
			</select></p>
			<p><label for="pessoa.cep">CEP</label><input class="inputform" id="pessoa.cep" name="pessoa.cep" type="text" value="${aluno.pessoa.cep}" size="9" maxlength="9"></p>
			<p><label for="pessoa.login">Login</label><input class="inputform" id="pessoa.login" name="pessoa.login" type="text" value="${aluno.pessoa.login}" size="45" maxlength="45"></p>
			<p><label for="pessoa.senha">Senha</label><input class="inputform" id="pessoa.senha" name="pessoa.senha" type="password" size="45" maxlength="45"></p>
			<p><label for="pessoa.email">Email</label><input class="inputform" id="pessoa.email" name="pessoa.email" type="text" value="${aluno.pessoa.email}" size="30" maxlength="100"></p>
			<p><label for="pessoa.telefoneCelular">Celular</label><input class="inputform" id="pessoa.telefoneCelular" name="pessoa.telefoneCelular" type="text" value="${aluno.pessoa.telefoneCelular}" size="15" maxlength="15"></p>
			<p><label for="pessoa.telefoneComercial">Telefone comercial</label><input class="inputform" id="pessoa.telefoneComercial" name="pessoa.telefoneComercial" type="text" value="${aluno.pessoa.telefoneComercial}" size="15" maxlength="15"></p>
			<p><label for="pessoa.telefoneResidencial">Telefone residencial</label><input class="inputform" id="pessoa.telefoneResidencial" name="pessoa.telefoneResidencial" type="text" value="${aluno.pessoa.telefoneResidencial}" size="15" maxlength="15"></p>
			<input type="hidden" name="idAluno" value="${aluno.idAluno}" />
			<p><label for="numCamisa">N�mero de camisa:</label>
			<select id="numCamisa" name="numCamisa">
				<option value="P" <c:if test="${aluno.numCamisa == 'P'}">selected="selected"</c:if>>P</option>
				<option value="M" <c:if test="${aluno.numCamisa == 'M'}">selected="selected"</c:if>>M</option>
				<option value="G" <c:if test="${aluno.numCamisa == 'G'}">selected="selected"</c:if>>G</option>
				<option value="GG" <c:if test="${aluno.numCamisa == 'GG'}">selected="selected"</c:if>>GG</option>
				<option value="XG" <c:if test="${aluno.numCamisa == 'XG'}">selected="selected"</c:if>>XG</option>
			</select></p>
			<c:choose>
				<c:when test="${alunoLogado.idAluno != null}">
					<p><label for="nivel.idNivel">N�vel</label>
					<select id="nivel.idNivel" disabled="disabled" name="nivel.idNivel">
						<c:forEach var="nivel" items="${niveis}">
						    <option value="${nivel.idNivel}" <c:if test="${aluno.nivel.idNivel == nivel.idNivel}">selected="selected"</c:if>>${nivel.idNivel} - ${nivel.nome}</option>
						</c:forEach>
					</select></p>
				</c:when>
				<c:when test="${professorLogado.idProfessor != null}">
					<p><label for="nivel.idNivel">N�vel</label>
					<select id="nivel.idNivel" name="nivel.idNivel">
						<c:forEach var="nivel" items="${niveis}">
						    <option value="${nivel.idNivel}" <c:if test="${aluno.nivel.idNivel == nivel.idNivel}">selected="selected"</c:if>>${nivel.idNivel} - ${nivel.nome}</option>
						</c:forEach>
					</select></p>
				</c:when>
			</c:choose>
			<input type="hidden" name="idAluno" value="${aluno.idAluno}" />
			<input type="hidden" name="grupo.idGrupo" value="${aluno.grupo.idGrupo}" />
			<p class="buttons">
				<button class="enviar">Salvar</button>
			</p>
		</form:form>
	</spring:nestedPath>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>