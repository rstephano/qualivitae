package br.com.qualivitae.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Usuario;

public class UsuarioDAO {
	private final Session session;

	public UsuarioDAO() {
		this.session = CriadorDeSession.getSession();
	}

	public int inserir(Usuario user) {
		Transaction tx = session.beginTransaction();
		session.save(user);
		tx.commit();
		session.close();
		return user.getIdUsuario();
	}

	public Usuario find(int id) {
		session.beginTransaction();
		Usuario user = (Usuario) session.load(Usuario.class, id);
		session.getTransaction().commit();
		return user;
	}
}