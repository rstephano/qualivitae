package br.com.qualivitae.model;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;

@Entity
@Table(name = "TreinosRealizados")
public class TreinoRealizado implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer idTreinoRealizado;
	@Column
	private Long realizado;
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "idAluno")
	private Aluno aluno;
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "idTreino")
	private Treino treino;

	public Integer getIdTreinoRealizado() {
		return idTreinoRealizado;
	}

	public void setIdTreinoRealizado(Integer idTreinoRealizado) {
		this.idTreinoRealizado = idTreinoRealizado;
	}

	public Long getRealizado() {
		return realizado;
	}

	public void setRealizado(Long realizado) {
		this.realizado = realizado;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Treino getTreino() {
		return treino;
	}

	public void setTreino(Treino treino) {
		this.treino = treino;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idTreinoRealizado == null) ? 0 : idTreinoRealizado
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreinoRealizado other = (TreinoRealizado) obj;
		if (idTreinoRealizado == null) {
			if (other.idTreinoRealizado != null)
				return false;
		} else if (!idTreinoRealizado.equals(other.idTreinoRealizado))
			return false;
		return true;
	}

}
