<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="header.jsp" />
<h2>Cadastro de usu&aacute;rio</h2>
<script type="text/javascript">
	$(document).ready(function() {
			$('#msg').fadeIn(1000);
	});
</script>
<div id="msg" class="${status.tipo}">
	<c:if test="${status.tipo == 'success'}">
		<p>Usu&aacute;rio ${param.nome} adicionado com sucesso.</p>
		<p>Clique <a href="?act=visualizar&idUsuario=${usuario.idUsuario}">aqui</a> para visualizar o cadastro.</p>
	</c:if>
	<c:if test="${status.tipo == 'error'}">
		<p>
			Ocorreu um erro ao cadastrar <b>${param.nome}</b>, tente novamente
			mais tarde.
		</p>
		<p id="btndetails">Detalhes:</p>
		<div id="details>">
			<p>
				<b>Erro ${status.codigo}</b>
			</p>
			<p>Mensagem: ${status.descricao}</p>
		</div>
	</c:if>
</div>

<c:import url="footer.jsp" />