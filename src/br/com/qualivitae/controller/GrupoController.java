package br.com.qualivitae.controller;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.qualivitae.dao.AlunoDAO;
import br.com.qualivitae.dao.GrupoDAO;
import br.com.qualivitae.dao.NivelDAO;
import br.com.qualivitae.dao.ProfessorDAO;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Nivel;
import br.com.qualivitae.model.Professor;

@Controller
@RequestMapping("/grupo/*")
public class GrupoController {
	@RequestMapping(value = "cadastro")
	public String formCadastro(ModelMap modelMap) {
		List<Professor> professores = new ProfessorDAO().findAll();
		modelMap.addAttribute("professores", professores);
		return "grupo/cadastro";
	}

	@RequestMapping(value = "cadastro", method = RequestMethod.POST)
	public String cadastrarGrupo(@Valid Grupo grupo, BindingResult result,
			ModelMap modelMap) {
		if (result.hasErrors()) {
			return "grupo/cadastro";
		}
		GrupoDAO grupoDao = new GrupoDAO();
		grupoDao.inserir(grupo);
		return "grupo/cadastro-realizado";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "todos")
	public String listarGruposTodos(ModelMap modelMap, HttpSession session) {
		Professor professor = (Professor) session.getAttribute("professorLogado");
		List<Grupo> grupos = null;
		if (professor.getAdministrador() == null) {
			grupos = new GrupoDAO().findAll(professor);
		} else {
			grupos = new GrupoDAO().findAll();
		}
		modelMap.addAttribute("grupos", grupos);
		return "grupo/listar-todos";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "{id}")
	public String visualizarGrupo(@PathVariable("id") int id, ModelMap modelMap) {
		List<Nivel> niveis = new NivelDAO().findAll();
		modelMap.addAttribute("niveis", niveis);
		Grupo grupo = new GrupoDAO().find(id);
		modelMap.addAttribute("grupo", grupo);
		List<Professor> professores = new ProfessorDAO().findAll();
		modelMap.addAttribute("professores", professores);
		List<Aluno> alunosNoGrupo = new AlunoDAO().findAllInGroup(id);
		modelMap.addAttribute("alunosnogrupo", alunosNoGrupo);
		List<Aluno> alunosForaDeGrupo = new AlunoDAO().findAllNotInGroup();
		modelMap.addAttribute("alunosforadegrupo", alunosForaDeGrupo);
		Calendar dataAtual = Calendar.getInstance();
		modelMap.addAttribute("hoje", dataAtual);
		return "grupo/visualizar";
	}

	@RequestMapping(value = "{id}", method = RequestMethod.POST)
	public String atualizarGrupo(@Valid Grupo grupo, BindingResult result,
			@PathVariable("id") int id, ModelMap modelMap) {
		if (result.hasErrors()) {
			return "grupo/visualizar";
		} else {
			GrupoDAO grupoDao = new GrupoDAO();
			grupoDao.atualizar(grupo);
			return "redirect:/grupo/" + id;
		}
	}

	@RequestMapping(value = "deletar/{id}")
	public String deletarGrupo(@PathVariable("id") int id) {
		GrupoDAO grupoDao = new GrupoDAO();
		grupoDao.deletar(id);
		return "redirect:/grupo/todos";
	}
}
