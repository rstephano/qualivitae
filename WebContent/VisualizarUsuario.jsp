<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:useBean id="gruposdao" class="br.com.qualivitae.model.GrupoDAO" />
<c:import url="header.jsp" />
<h2>Cadastro de usu&aacute;rio</h2>
<form action="cadastro?act=alterar" method="post" id="form_cadastro">
	<label for="idUsuario">Id</label> <input disabled="disabled" class="idUsuario" maxlength="10"
		size="10" id="idUsuario" type="text" name="idUsuario" value="${usuario.idUsuario}" />
	<label for="nome">Nome</label> <input disabled="disabled" class="inputform" maxlength="150"
		size="100" id="nome" type="text" name="nome" value="${usuario.nome}" />
	<label for="cpf">Cpf</label> <input disabled="disabled" class="inputform" maxlength="14"
		size="14" id="cpf" type="text" name="cpf" value="${usuario.cpf}" /> <label
		for="profissao">Profiss�o</label> <input class="inputform"
		maxlength="100" size="100" id="profissao" type="text" name="profissao"
		value="${usuario.profissao}" /> <label for="dataNascimento">Data
		de nascimento</label> <input disabled="disabled" class="inputform" maxlength="12" size="12"
		id="dataNascimento" type="text" name="dataNascimento"
		value="<fmt:formatDate value="${usuario.dataNascimento.time}" pattern="dd/MM/yyyy" />" /> <label for="endereco">Endere�o</label>
	<input class="inputform" maxlength="200" size="100" id="endereco"
		type="text" name="endereco" value="${usuario.endereco}" /> <label
		for="bairro">Bairro</label> <input class="inputform" maxlength="100"
		size="100" id="bairro" type="text" name="bairro"
		value="${usuario.bairro}" /> <label for="cep">CEP</label> <input
		class="inputform" maxlength="9" size="9" id="cep" type="text"
		name="cep" value="${usuario.cep}" /> <label for="login">Login</label>
	<input disabled="disabled" class="inputform" maxlength="45" size="45" id="login"
		type="text" name="login" value="${usuario.login}" /> <label
		for="senha">Senha</label> <input class="inputform" maxlength="45"
		size="45" id="senha" type="password" name="senha" value="${usuario.senha}" />
	<label for="email">Email</label> <input class="inputform"
		maxlength="45" size="45" id="email" type="text" name="email"
		value="${usuario.email}" /> <label for="telefoneCelular">Celular</label>
	<input class="inputform" maxlength="13" size="13" id="telefoneCelular"
		type="text" name="telefoneCelular" value="${usuario.telefoneCelular}" />
	<label for="telefoneComercial">Telefone comercial</label> <input
		class="inputform" maxlength="13" size="13" id="telefoneComercial"
		type="text" name="telefoneComercial"
		value="${usuario.telefoneComercial}" /> <label
		for="telefoneResidencial">Telefone residencial</label> <input
		class="inputform" maxlength="13" size="13" id="telefoneResidencial"
		type="text" name="telefoneResidencial"
		value="${usuario.telefoneResidencial}" />
		<select disabled="disabled" name="idGrupo">
		<c:forEach var="grupo" items="${gruposdao.lista}">
		<c:choose>
			<c:when test="${usuario.idGrupo == grupo.idGrupo}">
				<option selected="selected" value="${grupo.idGrupo}">${grupo.nome}</option>
			</c:when>
			<c:otherwise>
				<option value="${grupo.idGrupo}">${grupo.nome}</option>
			</c:otherwise>
		</c:choose>
		</c:forEach>
	</select>
	<button class="enviar">Salvar</button>
</form>
<c:import url="footer.jsp" />