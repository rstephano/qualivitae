<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Detalhe de Treino</h2>
<div class="content">
	<p>
		<b>Grupo:</b> ${grupo.nome} / <b>N�vel:</b> ${nivel.nome}
	</p>
	<p>
		<b>Data:</b>
		<fmt:formatDate value="${dataBase.time}" pattern="dd/MM/yyyy" />
	</p>
		<c:choose>
			<c:when test="${!empty treino.idTreino}">
				<c:if test="${alunoLogado != null}">
					<table id="detalhetreino">
						<tr>
							<td class="cabecalho">Tipo</td><td>${treino.tipo}</td>
						</tr>
						<tr>
							<td class="cabecalho">Descri��o</td><td>${treino.descricao}</td>
						</tr>
						<tr>
							<td class="cabecalho">Local</td><td>${treino.local}</td>
						</tr>
						<tr>
							<td class="cabecalho">Planejado</td><td>${treino.planejado}</td>
						</tr>
						<tr>
							<c:if test="${treinoRealizado.idTreinoRealizado != null}">
								<td class="cabecalho">Realizado</td><td>${treinoRealizado.realizado}</td>
							</c:if>
							<c:if test="${treinoRealizado.idTreinoRealizado == null}">
								<td class="cabecalho">Realizado</td>
								<td>
									<script type="text/javascript">
										$(document).ready(function(){
											$("form#treinoRealizado").submit(function() {
											// we want to store the values from the form input box, then send via ajax below
											var inputRealizado = document.getElementById('realizado');
											var realizado = inputRealizado.value;
											var idTreino = ${treino.idTreino};
											var idAluno = ${alunoLogado.idAluno};
												$.ajax({
													type: "POST",
													url: "${pageContext.servletContext.contextPath}/treino/treinorealizado",
													data: "realizado=" + realizado + "&treino.idTreino=" + idTreino + "&aluno.idAluno=" + idAluno,
													success: function(){
														$('form#treinoRealizado').hide();
														//$('form#submit :input').val("");
														document.getElementById('result').innerHTML = realizado;
														$('span.result').fadeIn();
													}
												});
											return false;
											});
										});
									</script>
									<spring:nestedPath path="treinoRealizado">
										<form:form action="" method="POST" commandName="treinoRealizado">
											<input id="realizado" name="realizado" type="text">
											<input id="aluno.idAluno" name="aluno.idAluno" type="hidden" value="${alunoLogado.idAluno}">
											<input id="treino.idTreino" name="treino.idTreino" type="hidden" value="${treino.idTreino}">
											<button type="submit">ok</button>
										</form:form>
									</spring:nestedPath>
									<span id="result" class="result" style="display:none;">OK</span>
								</td>
							</c:if>
						</tr>
					</table>
				</c:if>
				<c:if test="${professorLogado != null}">
					<table id="detalhetreino">
						<tr>
							<td class="cabecalho">Tipo</td><td>${treino.tipo}</td>
						</tr>
						<tr>
							<td class="cabecalho">Descri��o</td><td>${treino.descricao}</td>
						</tr>
						<tr>
							<td class="cabecalho">Local</td><td>${treino.local}</td>
						</tr>
						<tr>
							<td class="cabecalho">Planejado</td><td>${treino.planejado}</td>
						</tr>
					</table>
				</c:if>
			</c:when>
			<c:otherwise>
				<c:if test="${alunoLogado != null}">
					<div class="error">N�o h� treino previsto para esta data!!</div>
				</c:if>
				<c:if test="${professorLogado != null}">
					<form:form action="" method="POST" commandName="treino">
						<form:errors path="*">
							<div class="error">
								<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar realizar o cadastro:</b><br>
								<form:errors path="*" />
							</div>
						</form:errors>						
						<p><label for="tipo">Tipo de treino:</label><input class="inputform" id="tipo" name="tipo" type="text" value="${treino.tipo}" size="30" maxlength="45"></p>
						<p><label for="descricao">Descri��o:</label><textarea name="descricao" id="descricao" rows="5" cols="60">${treino.descricao}</textarea></p>
						<p><label for="local">Local do treino:</label><input class="inputform" id="local" name="local" type="text" value="${treino.local}" size="30" maxlength="45"></p>
						<p><label for="planejado">Planejado:</label><input class="inputform" id="planejado" name="planejado" type="text" value="${treino.planejado}" size="30" maxlength="45"></p>
						<input type="hidden" name="grupo.idGrupo" value="${grupo.idGrupo}">
						<input type="hidden" name="data" value="<fmt:formatDate value="${dataBase.time}" pattern="dd/MM/yyyy" />">
						<input type="hidden" name="nivel.idNivel" value="${nivel.idNivel}">
						<p class="buttons"><button class="enviar">Cadastrar Treino</button></p>					
					</form:form>
				</c:if>
			</c:otherwise>
		</c:choose>
	
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>