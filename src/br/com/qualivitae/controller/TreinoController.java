package br.com.qualivitae.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.qualivitae.dao.GrupoDAO;
import br.com.qualivitae.dao.NivelDAO;
import br.com.qualivitae.dao.TreinoDAO;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Nivel;
import br.com.qualivitae.model.Professor;
import br.com.qualivitae.model.Treino;
import br.com.qualivitae.model.TreinoRealizado;

@Controller
@RequestMapping("/treino/*")
public class TreinoController {
	@SuppressWarnings({ "unchecked", "null" })
	@RequestMapping(value = "{idgrupo}-{idnivel}", method = RequestMethod.GET)
	public String treinos(@PathVariable("idgrupo") int idgrupo,
			@PathVariable("idnivel") String idnivel, ModelMap modelMap,
			@RequestParam(value = "mes", required = false) int mes,
			@RequestParam(value = "ano", required = false) int ano,
			HttpSession session) throws ParseException {
		// Pegar o n�vel
		Nivel nivel = new NivelDAO().find(idnivel);
		modelMap.addAttribute("nivel", nivel);
		// Pegar o grupo
		Grupo grupo = new GrupoDAO().find(idgrupo);
		modelMap.addAttribute("grupo", grupo);
		// Pegar a data atual
		Calendar dataAtual = Calendar.getInstance();
		modelMap.addAttribute("hoje", dataAtual);
		// Pegar a data base
		Calendar dataBase = Calendar.getInstance();
		// Trocar o m�s de refer�ncia do calend�rio
		if ((mes >= 1 && mes <= 12)) {
			dataBase.set(Calendar.MONTH, mes - 1);
		}
		// Trocar o ano de refer�ncia do calend�rio
		dataBase.set(Calendar.YEAR, ano);
		// Enviar string com o nome do m�s atual
		int mesAtual = dataBase.get(Calendar.MONTH);
		String strMesAtual = "";
		switch (mesAtual) {
		case 0:
			strMesAtual = "Janeiro";
			break;
		case 1:
			strMesAtual = "Fevereiro";
			break;
		case 2:
			strMesAtual = "Mar�o";
			break;
		case 3:
			strMesAtual = "Abril";
			break;
		case 4:
			strMesAtual = "Maio";
			break;
		case 5:
			strMesAtual = "Junho";
			break;
		case 6:
			strMesAtual = "Julho";
			break;
		case 7:
			strMesAtual = "Agosto";
			break;
		case 8:
			strMesAtual = "Setembro";
			break;
		case 9:
			strMesAtual = "Outubro";
			break;
		case 10:
			strMesAtual = "Novembro";
			break;
		case 11:
			strMesAtual = "Dezembro";
			break;
		}
		modelMap.addAttribute("intMesAtual", mesAtual);
		modelMap.addAttribute("strMesAtual", strMesAtual);
		int anoAtual = dataBase.get(Calendar.YEAR);
		modelMap.addAttribute("intAnoAtual", anoAtual);
		if (mesAtual == 0) {
			modelMap.addAttribute("intMesAnterior", 12);
			modelMap.addAttribute("intAnoAnterior", anoAtual - 1);
		} else {
			modelMap.addAttribute("intMesAnterior", mesAtual);
			modelMap.addAttribute("intAnoAnterior", anoAtual);
		}
		if (mesAtual == 11) {
			modelMap.addAttribute("intMesPosterior", 1);
			modelMap.addAttribute("intAnoPosterior", anoAtual + 1);
		} else {
			modelMap.addAttribute("intMesPosterior", mesAtual + 2);
			modelMap.addAttribute("intAnoPosterior", anoAtual);
		}
		// Listar datas do calend�rio base
		List<Calendar> datasCalendario = new ArrayList<Calendar>();
		datasCalendario = listaDatas(dataBase);
		modelMap.addAttribute("datasCalendario", datasCalendario);
		modelMap.addAttribute("mesCorrente", dataBase.get(Calendar.MONTH));
		// Listar treinos
		List<Treino> treinos = new ArrayList<Treino>();
		TreinoDAO treinoDAO = new TreinoDAO();
		treinos = treinoDAO.listaTreinos(idgrupo, idnivel, dataBase);
		modelMap.addAttribute("treinos", treinos);
		return "treino/calendario";
	}

	@RequestMapping(value = "{idgrupo}-{idnivel}/view", method = RequestMethod.GET)
	public String treinos(@PathVariable("idgrupo") int idgrupo,
			@PathVariable("idnivel") String idnivel, ModelMap modelMap,
			@RequestParam(value = "dia") int dia,
			@RequestParam(value = "mes") int mes,
			@RequestParam(value = "ano") int ano, HttpSession session)
			throws ParseException {
		// Pegar o n�vel
		Nivel nivel = new NivelDAO().find(idnivel);
		modelMap.addAttribute("nivel", nivel);
		// Pegar o grupo
		Grupo grupo = new GrupoDAO().find(idgrupo);
		modelMap.addAttribute("grupo", grupo);
		// Data base
		Calendar dataBase = Calendar.getInstance();
		dataBase.set(ano, mes - 1, dia);
		modelMap.addAttribute("dataBase", dataBase);
		// Treino
		Treino treino = new Treino();
		TreinoDAO treinoDAO = new TreinoDAO();
		treino = treinoDAO.find(grupo.getIdGrupo(), nivel.getIdNivel(),
				dataBase);
		modelMap.addAttribute("treino", treino);
		// Aluno Logado
		Aluno aluno = (Aluno) session.getAttribute("alunoLogado");
		// TreinoRealizado
		if (aluno != null) {
			TreinoRealizado treinoRealizado = new TreinoRealizado();
			TreinoDAO treinoDAO2 = new TreinoDAO();
			treinoRealizado = treinoDAO2.findFeedBack(treino.getIdTreino(),
					aluno.getIdAluno());
			modelMap.addAttribute("treinoRealizado", treinoRealizado);
		}
		return "treino/visualizar";
	}

	@RequestMapping(value = "{idgrupo}-{idnivel}/view", method = RequestMethod.POST)
	public String cadastrarTreino(@Valid Treino treino, BindingResult result,
			@PathVariable("idgrupo") int idgrupo,
			@PathVariable("idnivel") String idnivel, ModelMap modelMap,
			@RequestParam(value = "dia") int dia,
			@RequestParam(value = "mes") int mes,
			@RequestParam(value = "ano") int ano, HttpSession session) {
		if (result.hasErrors()) {
			// Pegar o n�vel
			Nivel nivel = new NivelDAO().find(idnivel);
			modelMap.addAttribute("nivel", nivel);
			// Pegar o grupo
			Grupo grupo = new GrupoDAO().find(idgrupo);
			modelMap.addAttribute("grupo", grupo);
			// Data base
			Calendar dataBase = Calendar.getInstance();
			dataBase.set(ano, mes - 1, dia);
			modelMap.addAttribute("dataBase", dataBase);
			return "treino/visualizar";
		}
		TreinoDAO treinoDao = new TreinoDAO();
		treinoDao.inserir(treino);
		return "redirect:/treino/" + idgrupo + "-" + idnivel + "/view?dia="
				+ dia + "&mes=" + mes + "&ano=" + ano;
	}

	@RequestMapping(value = "treinorealizado", method = RequestMethod.POST)
	public String darFeedback(TreinoRealizado treinoRealizado, ModelMap modelMap) {
		TreinoDAO treinoDao = new TreinoDAO();
		treinoDao.darFeedBack(treinoRealizado);
		modelMap.addAttribute("treinoRealizado", treinoRealizado);
		return "treino/feedbackok";
	}

	public List<Calendar> listaDatas(Calendar dataBase) {
		// Pegar o primeiro domingo do calend�rio atual
		Calendar primeiroDiaDoCalendarioAtual = Calendar.getInstance();
		primeiroDiaDoCalendarioAtual.set(dataBase.get(Calendar.YEAR),
				dataBase.get(Calendar.MONTH),
				dataBase.getActualMinimum(Calendar.DAY_OF_MONTH));
		primeiroDiaDoCalendarioAtual.add(Calendar.DATE,
				-(primeiroDiaDoCalendarioAtual.get(Calendar.DAY_OF_WEEK) - 1));
		// Pegar o �ltimo sabado do calend�rio atual
		Calendar ultimoDiaDoCalendarioAtual = Calendar.getInstance();
		ultimoDiaDoCalendarioAtual.set(dataBase.get(Calendar.YEAR),
				dataBase.get(Calendar.MONTH),
				dataBase.getActualMaximum(Calendar.DAY_OF_MONTH));
		ultimoDiaDoCalendarioAtual.add(Calendar.DATE,
				7 - ultimoDiaDoCalendarioAtual.get(Calendar.DAY_OF_WEEK));
		// Montar o array de datas do calend�rio
		List<Calendar> datasCalendario = new ArrayList<Calendar>();
		do {
			Calendar newDate = Calendar.getInstance();
			newDate.set(primeiroDiaDoCalendarioAtual.get(Calendar.YEAR),
					primeiroDiaDoCalendarioAtual.get(Calendar.MONTH),
					primeiroDiaDoCalendarioAtual.get(Calendar.DATE));
			datasCalendario.add(newDate);
			primeiroDiaDoCalendarioAtual.add(Calendar.DATE, 1);
		} while (primeiroDiaDoCalendarioAtual.getTimeInMillis() <= ultimoDiaDoCalendarioAtual
				.getTimeInMillis());
		return datasCalendario;
	}
}
