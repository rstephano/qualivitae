<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Cadastro de Grupo</h2>
<div class="content">
	<spring:nestedPath path="grupo">
		<form:form action="" method="POST" commandName="grupo">
			<form:errors path="*">
				<div class="error">
					<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar realizar o cadastro:</b><br>
					<form:errors path="*" />
				</div>
			</form:errors>
			<p><label for="nome">Nome do grupo</label><input class="inputform" id="nome" name="nome" type="text" value="${grupo.nome}" size="30" maxlength="45"></p>
			<p><label for="descricao">Descri��o do Grupo</label><textarea name="descricao" id="descricao" rows="5" cols="60"></textarea></p>
			<p><label for="professor.idProfessor">Professor</label>
				<select id="professor.idProfessor" name="professor.idProfessor">
					<c:forEach var="professor" items="${professores}">
					    <option value="${professor.idProfessor}">${professor.pessoa.nome}</option>
					</c:forEach>
				</select>
			</p>
			<p class="buttons"><button class="enviar">Cadastrar Grupo</button></p>
		</form:form>
	</spring:nestedPath>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>