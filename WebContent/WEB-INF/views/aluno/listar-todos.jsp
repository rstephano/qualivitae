<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Alunos cadastrados</h2>
<script>
	function deletechecked(message) {
		var answer = confirm(message)
		if (answer) {
			document.messages.submit();
		}
		return false;
	}
</script>
<table class="list">
	<tr>
		<th>Id</th>
		<th>Nome</th>
		<th>UF</th>
		<th>A��es</th>
	</tr>
	<c:forEach var="aluno" items="${alunos}">
		<tr>
			<td>${aluno.idAluno}</td>
			<td>${aluno.pessoa.nome}</td>
			<td>${aluno.pessoa.uf}</td>
			<td><a title="Visualizar Aluno" href="<%=request.getContextPath()%>/aluno/${aluno.idAluno}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"></a> <a title="Deletar Aluno" onclick="return deletechecked('Voc� quer realmente deletar este aluno?');" href="<%=request.getContextPath()%>/aluno/deletar/${aluno.idAluno}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/101.png"></a></td>
		</tr>
	</c:forEach>
</table>
<%@ include file="/WEB-INF/views/footer.jsp"%>