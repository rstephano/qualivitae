package br.com.qualivitae.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.qualivitae.dao.AlunoDAO;
import br.com.qualivitae.dao.GrupoDAO;
import br.com.qualivitae.dao.NivelDAO;
import br.com.qualivitae.dao.ProfessorDAO;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Nivel;
import br.com.qualivitae.model.Professor;

@Controller
@RequestMapping("/professor/*")
public class ProfessorController {

	@RequestMapping(value = "cadastro")
	public String formCadastro() {
		return "professor/cadastro";
	}

	@RequestMapping(value = "cadastro", method = RequestMethod.POST)
	public String cadastrarProfessor(@Valid Professor professor,
			BindingResult result, ModelMap modelMap) {
		if (result.hasErrors()) {
			return "professor/cadastro";
		}
		ProfessorDAO professorDao = new ProfessorDAO();
		professorDao.inserir(professor);
		return "professor/cadastro-realizado";
	}

	@RequestMapping(value = "todos")
	public String listarProfessoresTodos(ModelMap modelMap) {
		List<Professor> professores = new ProfessorDAO().findAll();
		modelMap.addAttribute("professores", professores);
		return "professor/listar-todos";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "{id}")
	public String visualizarProfessor(@PathVariable("id") int id,
			ModelMap modelMap) {
		/*
		 * List<Grupo> grupos = new GrupoDAO().findAll();
		 * modelMap.addAttribute("grupos", grupos);
		 */
		Professor professor = new ProfessorDAO().find(id);
		modelMap.addAttribute("professor", professor);
		return "professor/visualizar";
	}

	@RequestMapping(value = "{id}", method = RequestMethod.POST)
	public String atualizarProfessor(@Valid Professor professor,
			BindingResult result, @PathVariable("id") int id, ModelMap modelMap) {
		/*if (result.hasErrors()) {
			return "professor/visualizar";
		} else {
			ProfessorDAO professorDao = new ProfessorDAO();
			professorDao.atualizar(professor);
			return "redirect:/professor/" + id;
		}*/
		if (result.hasErrors()) {
			if (result.getFieldValue("pessoa.senha") == "" & result.getErrorCount() == 2) {
				ProfessorDAO professorDao = new ProfessorDAO();
				Professor professorAntigo = professorDao.find(professor.getIdProfessor());
				professor.getPessoa().setSenha(professorAntigo.getPessoa().getSenha());
				professorDao.atualizar(professor);
				return "redirect:/professor/" + id;
			} else {
				return "professor/visualizar";
			}
		} else {
			ProfessorDAO professorDao = new ProfessorDAO();
			professorDao.atualizar(professor);
			return "redirect:/professor/" + id;
		}
	}

	@RequestMapping(value = "deletar/{id}")
	public String deletarProfessor(@PathVariable("id") int id) {
		ProfessorDAO professorDao = new ProfessorDAO();
		professorDao.deletar(id);
		return "redirect:/professor/todos";
	}
}
