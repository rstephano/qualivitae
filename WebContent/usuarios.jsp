<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="header.jsp" />
<body>
	<jsp:useBean id="dao" class="br.com.qualivitae.dao.UsuarioDAO" />
	<table>
		<tr>
			<td>Id</td>
			<td>Nome</td>
			<td>Login</td>
			<td>CPF</td>
			<td>Email</td>
		</tr>
		<c:forEach var="usuario" items="${dao.lista}">
			<tr>
				<td>${usuario.id}</td>
				<td>${usuario.nome}</td>
				<td>${usuario.login}</td>
				<td>${usuario.cpf}</td>
				<td>${usuario.email}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>