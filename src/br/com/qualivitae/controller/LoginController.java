package br.com.qualivitae.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.qualivitae.dao.AlunoDAO;
import br.com.qualivitae.dao.ProfessorDAO;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Pessoa;
import br.com.qualivitae.model.Professor;

@Controller
@RequestMapping("login")
public class LoginController {
	@RequestMapping(method = RequestMethod.GET)
	public String formLoginAluno(HttpSession session){
		session.setAttribute("loginmessage", "null");
		return "login/formlogin";
	}
	/*@RequestMapping(method = RequestMethod.POST)
	public String processaLoginAluno(Aluno alunoLogado, HttpSession session){
		Aluno aluno = new AlunoDAO().find(alunoLogado.getPessoa().getLogin(),alunoLogado.getPessoa().getSenha());
		if (aluno.getIdAluno() == null) {
			session.setAttribute("loginmessage", "N�o foi poss�vel efetuar o login. Tente novamente");
			return "login/formlogin";
		} else {
			session.setAttribute("alunoLogado", aluno);
			return "redirect:index";
		}
	}*/
	@RequestMapping(method = RequestMethod.POST)
	public String processaLogin(Pessoa pessoa, HttpSession session){
		Aluno aluno = new AlunoDAO().find(pessoa.getLogin(),pessoa.getSenha());
		if (aluno.getIdAluno() == null) {
			Professor professor = new ProfessorDAO().find(pessoa.getLogin(),pessoa.getSenha());
			if (professor.getIdProfessor() == null) {
				session.setAttribute("loginmessage", "N�o foi poss�vel efetuar o login. Tente novamente");
				return "login/formlogin";
			}else{
				session.setAttribute("professorLogado", professor);
				return "redirect:index";
			}
		} else {
			session.setAttribute("alunoLogado", aluno);
			return "redirect:index";
		}
	}
	@RequestMapping(value = "professor", method = RequestMethod.POST)
	public String processaLoginProfessor(Professor professorLogado, HttpSession session){
		Professor professor = new ProfessorDAO().find(professorLogado.getPessoa().getLogin(),professorLogado.getPessoa().getSenha());
		if (professor.getIdProfessor() == null) {
			session.setAttribute("loginmessage", "N�o foi poss�vel efetuar o login. Tente novamente");
			return "login/formlogin";
		} else {
			session.setAttribute("professorLogado", professor);
			return "redirect:index";
		}
	}
	@RequestMapping(value = "logout")
	public String processaLogout(ModelMap modelMap, HttpSession session){
		session.removeAttribute("alunoLogado");
		session.removeAttribute("professorLogado");
		session.setAttribute("loginmessage", "Voce foi deslogado do QualiVitae");
		return "login/formlogin";
	}
}
