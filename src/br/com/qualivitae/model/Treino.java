package br.com.qualivitae.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Treinos")
public class Treino implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer idTreino;
	@ManyToOne()
	@JoinColumn(name = "idGrupo")
	private Grupo grupo;
	@Column
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "A data deve ser preenchida, utilize o padr�o dd/mm/aaaa")
	private Calendar data;
	@Column
	@NotNull(message = "Favor preencher o tipo de treino")
	private String tipo;
	@Column
	@NotNull(message = "Favor preencher a descri��o do treino")
	private String descricao;
	@Column
	@NotNull(message = "Favor preencher o local")
	private String local;
	@Column
	@NotNull(message = "Favor preencher o planejado")
	private float planejado;
	@Valid
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "idNivel")
	private Nivel nivel;
	@OneToMany(mappedBy = "treino")
	private List<TreinoRealizado> treinosRealizados;

	public Treino() {

	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getIdTreino() {
		return idTreino;
	}

	public void setIdTreino(Integer idTreino) {
		this.idTreino = idTreino;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public float getPlanejado() {
		return planejado;
	}

	public void setPlanejado(float planejado) {
		this.planejado = planejado;
	}

	public Nivel getNivel() {
		return nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public List<TreinoRealizado> getTreinosRealizados() {
		return treinosRealizados;
	}

	public void setTreinosRealizados(List<TreinoRealizado> treinosRealizados) {
		this.treinosRealizados = treinosRealizados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idTreino == null) ? 0 : idTreino.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Treino other = (Treino) obj;
		if (idTreino == null) {
			if (other.idTreino != null)
				return false;
		} else if (!idTreino.equals(other.idTreino))
			return false;
		return true;
	}
}
