<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>DashBoard</h2>
<div class="content">
	<c:choose>
		<c:when test="${alunoLogado.idAluno != null}">
			<h3>Histórico</h3>
			<div id="chart_div" style="width: 660px; height: 200px;"></div>
		</c:when>
		<c:when test="${professorLogado.idProfessor != null}">
			<h3>Dashboard de Professor</h3>
			<div id="chart_div" style="width: 660px; height: 200px;"></div>
		</c:when>
	</c:choose>
</div>
	<c:choose>
		<c:when test="${alunoLogado.idAluno != null}">
			<script type="text/javascript" src="https://www.google.com/jsapi"></script>
			<script type="text/javascript">
				google.load("visualization", "1", {
					packages : [ "corechart" ]
				});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
					var data = new google.visualization.DataTable();
					data.addColumn('date', 'Data');
					data.addColumn('number', 'Previsto');
					data.addColumn('number', 'Realizado');
					data.addRows([
						<c:forEach var="treino" items="${treinos}" varStatus="id">
						<c:set var="control" value="0" scope="request" />
						<c:forEach var="treinoRealizado" items="${treinosRealizados}">
							<c:if test="${treinoRealizado.treino.idTreino == treino.idTreino}">
								<c:set var="control" value="1" scope="request" />
								[new Date(<fmt:formatDate value="${treino.data.time}" pattern="yyyy" />, <fmt:formatDate value="${treino.data.time}" pattern="MM" />, <fmt:formatDate value="${treino.data.time}" pattern="dd" />), ${treino.planejado}, ${treinoRealizado.realizado}],
							</c:if>
						</c:forEach>
						<c:if test="${control == 0}">
							[new Date(<fmt:formatDate value="${treino.data.time}" pattern="yyyy" />, <fmt:formatDate value="${treino.data.time}" pattern="MM" />, <fmt:formatDate value="${treino.data.time}" pattern="dd" />), ${treino.planejado}, null],
						</c:if>
						</c:forEach>
					]);
		
					var options = {
						title : 'Histórico de Treinos',
						legend: 'none',
						hAxis: {title: 'Data'},
						pointSize: 5
					};
		
					var chart = new google.visualization.LineChart(document
							.getElementById('chart_div'));
					chart.draw(data, options);
				}
			</script>
		</c:when>
	</c:choose>
<%@ include file="/WEB-INF/views/footer.jsp"%>