<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Cadastro do grupo ${grupo.nome}</h2>
	<div id="toolbar">
		<ul id="menu">
			<li><a href="#">Visualizar / Editar Treinos</a>
				<div><ul>
					<c:forEach var="nivel" items="${niveis}">
					    <li><a href="<%=request.getContextPath()%>/treino/${grupo.idGrupo}-${nivel.idNivel}?mes=${hoje.time.month+1}&ano=${1900+hoje.time.year}" class="parent">${nivel.nome}</a></li>
					</c:forEach>
				</ul></div>
			</li>
		</ul>
	</div>
	<div class="clear"></div>
	<div class="content">
	<spring:nestedPath path="grupo">
		<form:form action="${pageContext.request.contextPath}/grupo/${grupo.idGrupo}" method="POST" commandName="grupo">
			<form:errors path="*">
				<div class="error">
					<b>Ocorreram o(s) seguinte(s) erro(s) ao tentar atualizar o cadastro:</b><br>
					<form:errors path="*" />
				</div>
			</form:errors>
			<input type="hidden" name="idGrupo" value="${grupo.idGrupo}">
			<p>
				<label for="nome">Nome do grupo</label><input class="inputform" id="nome" name="nome" type="text" value="${grupo.nome}" size="30" maxlength="45">
			</p>
			<p>
				<label for="descricao">Descri��o do Grupo</label>
				<textarea name="descricao" id="descricao" rows="5" cols="60">${grupo.descricao}</textarea>
			</p>
			<p>
				<c:choose>
					<c:when test="${alunoLogado.idAluno != null || professorLogado.administrador != 1}">
						<label for="professor.idProfessor">Professor</label> <select id="professor.idProfessor" disabled="disabled" name="professor.idProfessor">
							<c:forEach var="professor" items="${professores}">
								<option value="${professor.idProfessor}" <c:if test="${professor.idProfessor == grupo.professor.idProfessor}">selected="selected"</c:if>>${professor.pessoa.nome}</option>
							</c:forEach>
						</select>
					</c:when>
					<c:when test="${professorLogado.administrador == 1}">
						<label for="professor.idProfessor">Professor</label> <select id="professor.idProfessor" name="professor.idProfessor">
							<c:forEach var="professor" items="${professores}">
								<option value="${professor.idProfessor}" <c:if test="${professor.idProfessor == grupo.professor.idProfessor}">selected="selected"</c:if>>${professor.pessoa.nome}</option>
							</c:forEach>
					</select>
				</c:when>
				</c:choose>
			</p>
			<p class="buttons">
				<button class="enviar">Salvar</button>
			</p>
		</form:form>
	</spring:nestedPath>
	<h3>Alunos deste grupo</h3>
	<p><form:form action="${pageContext.request.contextPath}/aluno/addgrupo" method="POST" commandName="aluno">
			<input type="hidden" name="grupo.idGrupo" value="${grupo.idGrupo}">
			<select id="idAluno" name="idAluno">
				<c:forEach var="alunoforadegrupo" items="${alunosforadegrupo}">
					<option value="${alunoforadegrupo.idAluno}" >${alunoforadegrupo.pessoa.nome}</option>
				</c:forEach>
			</select>
			<button class="enviar">Adicionar</button>
		</form:form>
	</p>
	<table class="list">
		<tr>
			<th>Id</th>
			<th>Nome</th>
			<th>A��es</th>
		</tr>
		<c:forEach var="alunonogrupo" items="${alunosnogrupo}">
			<tr>
				<td>${alunonogrupo.idAluno}</td>
				<td>${alunonogrupo.pessoa.nome}</td>
				<td><a title="Visualizar Aluno" href="<%=request.getContextPath()%>/aluno/${alunonogrupo.idAluno}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"></a><a title="Remover Aluno" onclick="return deletechecked('Voc� quer realmente remover este aluno do grupo?');" href="<%=request.getContextPath()%>/aluno/removergrupo/${alunonogrupo.idAluno}/${grupo.idGrupo}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/101.png"></a></td>
			</tr>
		</c:forEach>
	</table>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>