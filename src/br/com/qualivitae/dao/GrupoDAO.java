package br.com.qualivitae.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Professor;

public class GrupoDAO {
	private final Session session;

	public GrupoDAO() {
		this.session = CriadorDeSession.getSession();
	}

	public Grupo find(int id) {
		session.beginTransaction();
		Grupo grupo = (Grupo) session.get(Grupo.class, id);
		session.getTransaction().commit();
		return grupo;
	}

	public List findAll() {
		session.beginTransaction();
		List grupos = session.createQuery("from Grupo").list();
		session.getTransaction().commit();
		return grupos;
	}
	
	public List findAll(Professor professor) {
		session.beginTransaction();
		List grupos = session.createQuery("from Grupo where idProfessor = " + professor.getIdProfessor()).list();
		session.getTransaction().commit();
		return grupos;
	}
	
	public Integer inserir(Grupo grupo) {
		Transaction tx = session.beginTransaction();
		session.save(grupo);
		tx.commit();
		session.close();
		return grupo.getIdGrupo();
	}

	public void atualizar(Grupo grupo) {
		Transaction tx = session.beginTransaction();
		session.update(grupo);
		tx.commit();
		session.close();
	}

	public void deletar(int id) {
		Grupo grupo = new GrupoDAO().find(id);
		session.beginTransaction();
		session.delete(grupo);
		session.getTransaction().commit();
	}

}
