<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Professores cadastrados</h2>
<script>
	function deletechecked(message) {
		var answer = confirm(message)
		if (answer) {
			document.messages.submit();
		}
		return false;
	}
</script>
<table class="list">
	<tr>
		<th>Id</th>
		<th>Nome</th>
		<th>UF</th>
		<th>A��es</th>
	</tr>
	<c:forEach var="professor" items="${professores}">
		<tr>
			<td>${professor.idProfessor}</td>
			<td>${professor.pessoa.nome}</td>
			<td>${professor.pessoa.uf}</td>
			<td><a title="Visualizar Professor" href="<%=request.getContextPath()%>/professor/${professor.idProfessor}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"></a> <a title="Deletar Professor" onclick="return deletechecked('Voc� quer realmente deletar este professor?');" href="<%=request.getContextPath()%>/professor/deletar/${professor.idProfessor}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/101.png"></a></td>
		</tr>
	</c:forEach>
</table>
<%@ include file="/WEB-INF/views/footer.jsp"%>