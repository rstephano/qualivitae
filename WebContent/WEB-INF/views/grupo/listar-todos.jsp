<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Grupos cadastrados</h2>
<script>
	function deletechecked(message) {
		var answer = confirm(message)
		if (answer) {
			document.messages.submit();
		}
		return false;
	}
</script>
<table class="list">
	<tr>
		<th>Id</th>
		<th>Nome</th>
		<th>Descri��o</th>
		<th>Professor</th>
		<th>A��es</th>
	</tr>
	<c:forEach var="grupo" items="${grupos}">
		<tr>
			<td>${grupo.idGrupo}</td>
			<td>${grupo.nome}</td>
			<td>${grupo.descricao}</td>
			<td>${grupo.professor.pessoa.nome}</td>
			<td><a title="Administrar Grupo" href="<%=request.getContextPath()%>/grupo/${grupo.idGrupo}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/041.png"></a> <a title="Deletar Grupo" onclick="return deletechecked('Voc� quer realmente deletar este grupo?');" href="<%=request.getContextPath()%>/grupo/deletar/${grupo.idGrupo}"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/101.png"></a></td>
		</tr>
	</c:forEach>
</table>
<%@ include file="/WEB-INF/views/footer.jsp"%>