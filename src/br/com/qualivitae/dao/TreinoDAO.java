package br.com.qualivitae.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Grupo;
import br.com.qualivitae.model.Treino;
import br.com.qualivitae.model.TreinoRealizado;

public class TreinoDAO {
	private final Session session;

	public TreinoDAO() {
		this.session = CriadorDeSession.getSession();
	}

	public Treino find(int idTreino) {
		session.beginTransaction();
		Treino treino = (Treino) session.get(Treino.class, idTreino);
		session.getTransaction().commit();
		return treino;
	}

	public Treino find(int idGrupo, String idNivel, Calendar data) {
		session.beginTransaction();
		int dia = data.get(Calendar.DAY_OF_MONTH);
		int mes = data.get(Calendar.MONTH) + 1;
		int ano = data.get(Calendar.YEAR);
		List<Treino> treinos = session.createQuery(
				"from Treino where (idGrupo = " + idGrupo + ") and (idNivel ='"
						+ idNivel + "') and (data='" + ano + "-" + mes + "-"
						+ dia + "')").list();
		Treino treino = new Treino();
		for (Treino treinotemp : (List<Treino>) treinos) {
			treino = find(treinotemp.getIdTreino());
		}
		return treino;
	}

	public TreinoRealizado findFeedBack(int idTreino, int idAluno) {
		session.beginTransaction();
		List<TreinoRealizado> treinosRealizados = session.createQuery(
				"from TreinoRealizado where idAluno = " + idAluno
						+ " AND idTreino = " + idTreino).list();
		TreinoRealizado treinoRealizado = new TreinoRealizado();
		for (TreinoRealizado treinoRealizadotemp : (List<TreinoRealizado>) treinosRealizados) {
			treinoRealizado = treinoRealizadotemp;
		}
		return treinoRealizado;
	}

	public TreinoRealizado darFeedBack(TreinoRealizado treinoRealizado) {
		Transaction tx = session.beginTransaction();
		session.save(treinoRealizado);
		tx.commit();
		session.close();
		return treinoRealizado;
	}

	public List<Treino> listaTreinos(int idGrupo, String idNivel,
			Calendar dataBase) {
		session.beginTransaction();
		// Pegar o primeiro domingo do calendário atual
		Calendar primeiroDiaDoCalendarioAtual = Calendar.getInstance();
		primeiroDiaDoCalendarioAtual.set(dataBase.get(Calendar.YEAR),
				dataBase.get(Calendar.MONTH),
				dataBase.getActualMinimum(Calendar.DAY_OF_MONTH));
		primeiroDiaDoCalendarioAtual.add(Calendar.DATE,
				-(primeiroDiaDoCalendarioAtual.get(Calendar.DAY_OF_WEEK) - 1));
		// Pegar o último sabado do calendário atual
		Calendar ultimoDiaDoCalendarioAtual = Calendar.getInstance();
		ultimoDiaDoCalendarioAtual.set(dataBase.get(Calendar.YEAR),
				dataBase.get(Calendar.MONTH),
				dataBase.getActualMaximum(Calendar.DAY_OF_MONTH));
		ultimoDiaDoCalendarioAtual.add(Calendar.DATE,
				7 - ultimoDiaDoCalendarioAtual.get(Calendar.DAY_OF_WEEK));
		// Montar o array de datas do calendário
		int primeiroDia = primeiroDiaDoCalendarioAtual
				.get(Calendar.DAY_OF_MONTH);
		int primeiroMes = primeiroDiaDoCalendarioAtual.get(Calendar.MONTH) + 1;
		int primeiroAno = primeiroDiaDoCalendarioAtual.get(Calendar.YEAR);
		int ultimoDia = ultimoDiaDoCalendarioAtual.get(Calendar.DAY_OF_MONTH);
		int ultimoMes = ultimoDiaDoCalendarioAtual.get(Calendar.MONTH) + 1;
		int ultimoAno = ultimoDiaDoCalendarioAtual.get(Calendar.YEAR);
		List<Treino> treinos = session.createQuery(
				"from Treino where (idGrupo = " + idGrupo + ") and (idNivel ='"
						+ idNivel + "') and (data>='" + primeiroAno + "-"
						+ primeiroMes + "-" + primeiroDia + "') and (data<='"
						+ ultimoAno + "-" + ultimoMes + "-" + ultimoDia
						+ "') order by data asc").list();
		session.getTransaction().commit();
		return treinos;
	}

	public List<TreinoRealizado> listaTreinosRealizados(Aluno aluno, Integer idGrupo, String idNivel, Calendar dataBase, Integer amount) {
		session.beginTransaction();
		int ultimoDia = dataBase.get(Calendar.DAY_OF_MONTH);
		int ultimoMes = dataBase.get(Calendar.MONTH) + 1;
		int ultimoAno = dataBase.get(Calendar.YEAR);
		Calendar dataInicio = dataBase;
		List<TreinoRealizado> treinosRealizados = session.createQuery("from TreinoRealizado where (idAluno = " + aluno.getIdAluno() + ") order by idTreino desc").setMaxResults(amount).list();
		session.getTransaction().commit();
		return treinosRealizados;
	}
	public List<Treino> listaTreinos(Integer idGrupo, String idNivel,
			Calendar dataBase, Integer amount) {
		session.beginTransaction();
		int ultimoDia = dataBase.get(Calendar.DAY_OF_MONTH);
		int ultimoMes = dataBase.get(Calendar.MONTH) + 1;
		int ultimoAno = dataBase.get(Calendar.YEAR);
		Calendar dataInicio = dataBase;
		List<Treino> treinos = session.createQuery(
			"from Treino where (idGrupo = " + idGrupo + ") and (idNivel ='"
					+ idNivel + "') and (data<='"
					+ ultimoAno + "-" + ultimoMes + "-" + ultimoDia
					+ "') order by data desc").setMaxResults(amount).list();
		session.getTransaction().commit();
		return treinos;
	}

	public Integer inserir(Treino treino) {
		Transaction tx = session.beginTransaction();
		session.save(treino);
		tx.commit();
		session.close();
		return treino.getIdTreino();
	}

}
