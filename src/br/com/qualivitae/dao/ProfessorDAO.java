package br.com.qualivitae.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.qualivitae.infra.CriadorDeSession;
import br.com.qualivitae.model.Aluno;
import br.com.qualivitae.model.Pessoa;
import br.com.qualivitae.model.Professor;

public class ProfessorDAO {
	private final Session session;

	public ProfessorDAO() {
		this.session = CriadorDeSession.getSession();
	}

	public Integer inserir(Professor professor) {
		Transaction tx = session.beginTransaction();
		session.save(professor);
		tx.commit();
		session.close();
		return professor.getIdProfessor();
	}

	public List<Professor> findAll() {
		session.beginTransaction();
		List<Professor> professores = session.createQuery("from Professor").list();
		session.getTransaction().commit();
		return professores;
	}

	public Professor find(int id) {
		session.beginTransaction();
		Professor professor = (Professor) session.get(Professor.class, id);
		session.getTransaction().commit();
		return professor;
	}

	public void atualizar(Professor professor) {
		Transaction tx = session.beginTransaction();
		session.clear();
		session.update(professor);
		tx.commit();
	}

	public void deletar(int id) {
		Professor professor = new ProfessorDAO().find(id);
		session.beginTransaction();
		session.delete(professor);
		session.getTransaction().commit();
	}

	public Professor find(String login, String senha) {
		session.beginTransaction();
		Pessoa pessoa = new PessoaDAO().find(login, senha);
		List<Professor> professores = session.createQuery("from Professor where idPessoa=" + pessoa.getIdPessoa()).list();
		Professor professor = new Professor();
		for (Professor professortemp : (List<Professor>) professores) {
			professor = professortemp;
		}
		session.getTransaction().commit();
		return professor;
	}

}
