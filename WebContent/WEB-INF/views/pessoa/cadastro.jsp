<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>QualiVitae</title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/reset.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/960.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/960_16_col.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/text.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/forms.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/smoothness/jquery-ui-1.8.20.custom.css">
</head>
<body>
	<div class="container_16" id="top">
		<div class="grid_4" id="logo"></div>
		<div class="grid_7">&nbsp;</div>
		<div class="grid_5">
			<c:choose>
				<c:when test="${alunoLogado.idAluno != null}">
					<p class="usertoolbar"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"> ${alunoLogado.pessoa.nome} <a href="<%=request.getContextPath()%>/login/logout"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/10/151.png"></a></p>
				</c:when>
				<c:when test="${professorLogado.idProfessor != null}">
					<p class="usertoolbar"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"> ${professorLogado.pessoa.nome} <a href="<%=request.getContextPath()%>/login/logout"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/10/151.png"></a></p>
				</c:when>
				<c:otherwise>
					<a href="<%=request.getContextPath()%>/cadastro" class="join">Cadastre-se agora!</a>
					<a class="loginlink" href="<%=request.getContextPath()%>/login">Ou efetue o Login</a>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="clear"></div>
<div class="grid_16" id="content">
<h2>Cadastro no QualiVitae</h2>
<div class="content">
	<p>Por gentileza, escolha entre Aluno e Professor para realizar o cadastro.</p>
	<p><a class="big-button" href="<%=request.getContextPath()%>/aluno/cadastro/">Cadastrar Aluno</a><a class="big-button" href="<%=request.getContextPath()%>/professor/cadastro/">Cadastrar Professor</a></p>
</div>
<%@ include file="/WEB-INF/views/footer.jsp"%>