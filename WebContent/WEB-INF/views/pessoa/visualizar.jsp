<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ include file="/WEB-INF/views/header.jsp"%>
<h2>Visualizar usu&aacute;rio</h2>
<ul>
	<li>Nome</li>
	<li>${usuario.nome}</li>
	<li>Cpf</li>
	<li>${usuario.cpf}</li>
	<li>Profiss�o</li>
	<li>${usuario.profissao}</li>
	<li>Data de nascimento</li>
	<li><fmt:formatDate value="${usuario.dataNascimento.time}" pattern="dd/MM/yyyy" /></li>
	<li>Endere�o</li>
	<li>${usuario.endereco}"</li>
	<li>Bairro</li>
	<li>${usuario.bairro}</li>
	<li>Cidade</li>
	<li>${usuario.cidade}</li>
	<li>CEP</li>
	<li>${usuario.cep}</li>
	<li>Login</li>
	<li>${usuario.login}</li>
	<li>Email</li>
	<li>${usuario.email}</li>
	<li>Celular</li>
	<li>${usuario.telefoneCelular}</li>
	<li>Telefone comercial</li>
	<li>${usuario.telefoneComercial}</li>
	<li>Telefone residencial</li>
	<li>${usuario.telefoneResidencial}</li>
	<li>Grupo</li>
	<li><c:forEach var="grupo" items="${grupos}">
			<c:choose>
				<c:when test="${usuario.idGrupo == grupo.idGrupo}">
							${grupo.idGrupo} - ${grupo.nome}
						</c:when>
			</c:choose>
		</c:forEach></li>
</ul>
<%@ include file="/WEB-INF/views/footer.jsp"%>