<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>QualiVitae</title>
	<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jquery.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/reset.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/960.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/960_16_col.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/text.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/forms.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/style.css">
</head>
<body>
	<div class="container_16" id="top">
		<div class="grid_4" id="logo"></div>
		<div class="grid_7">&nbsp;</div>
		<div class="grid_5">
			<c:choose>
				<c:when test="${alunoLogado.idAluno != null}">
					<p><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"> ${alunoLogado.pessoa.nome} <a href="<%=request.getContextPath()%>/login/logout"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/10/151.png"></a></p>
				</c:when>
				<c:when test="${professorLogado.idProfessor != null}">
					<p class="usertoolbar"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/16/005.png"> ${professorLogado.pessoa.nome} <a href="<%=request.getContextPath()%>/login/logout"><img src="<%=request.getContextPath()%>/resources/img/icons_diagona/10/151.png"></a></p>
				</c:when>
				<c:otherwise>
					<a href="<%=request.getContextPath()%>/cadastro" class="join">Cadastre-se agora!</a>
					<a href="<%=request.getContextPath()%>/login">Ou efetue o Login</a>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="clear"></div>
	</div>
	<div class="container_16" id="logincontainer">
		<div class="grid_8 formlogin">
			<p>&nbsp;</p>
		</div>
		<div class="grid_8 formlogin">
			<h2>Login</h2>
			<c:choose>
				<c:when test="${alunoLogado.idAluno != null}">
					<div class="error">${alunoLogado.pessoa.nome}, Voc� j� est� logado no QualiVitae</div>
				</c:when>
				<c:when test="${professorLogado.idProfessor != null}">
					<div class="error">Professor ${professorLogado.pessoa.nome}, Voc� j� est� logado no QualiVitae</div>
				</c:when>
				<c:otherwise>
					<form:form action="${pageContext.servletContext.contextPath}/login" method="POST" commandName="pessoa">
						<c:if test="${loginmessage != 'null'}">
							<div class="error">${loginmessage}</div>
						</c:if>
						<p><label for="login">Login</label><input class="inputform" id="login" name="login" type="text" size="30" maxlength="45"></p>
						<p><label for="senha">Senha</label><input class="inputform" id="senha" name="senha" type="password" size="30" maxlength="45"></p>
						<p class="buttons"><button class="enviar">Efetuar Login</button></p>
					</form:form>
				</c:otherwise>
			</c:choose>
<%@ include file="/WEB-INF/views/footer.jsp"%>